//-----------------------------------------------------------------------
// <Copyright file="SysRoleOrgRepository.cs" company="RuYiAdmin">
// * Copyright (C) 2021 RuYiAdmin All Rights Reserved
// * Version : 4.0.30319.42000
// * Author  : auto generated by RuYiAdmin T4 Template
// * FileName: SysRoleOrgRepository.cs
// * History : Created by RuYiAdmin 11/08/2021 10:10:52
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.BaseRepository.Implementation;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface;

namespace RuYiAdmin.Net.RepositoryLayer.DataRepository.Implementation.SystemImplementation
{
    /// <summary>
    /// 角色机构数据访问层实现
    /// </summary>   
    public class SysRoleOrgRepository : RuYiAdminBaseRepository<SysRoleOrg>, ISysRoleOrgRepository
    {
        /// <summary>
        /// HttpContext
        /// </summary>
        private readonly IHttpContextAccessor context;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="context"></param>
        public SysRoleOrgRepository(IHttpContextAccessor context) : base(context)
        {
            this.context = context;
        }
    }
}
