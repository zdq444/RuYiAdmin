﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.BaseRepository.Interface;

namespace RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface
{
    /// <summary>
    /// 机构数据访问层接口
    /// </summary>
    public interface ISysOrganizationRepository : IRuYiAdminBaseRepository<SysOrganization>
    {
    }
}
