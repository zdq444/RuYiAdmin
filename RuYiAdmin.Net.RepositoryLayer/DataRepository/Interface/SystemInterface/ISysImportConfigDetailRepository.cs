﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.BaseRepository.Interface;

namespace RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface
{
    /// <summary>
    /// 导入配置明细数据访问层接口
    /// </summary>
    public interface ISysImportConfigDetailRepository : IRuYiAdminBaseRepository<SysImportConfigDetail>
    {
    }
}
