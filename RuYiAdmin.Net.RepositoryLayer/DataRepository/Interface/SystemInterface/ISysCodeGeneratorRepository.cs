﻿using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.FrameworkModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface
{
    /// <summary>
    /// 代码生成器数据访问层接口
    /// </summary>
    public interface ISysCodeGeneratorRepository
    {
        /// <summary>
        /// 获取表名称列表
        /// </summary>
        /// <returns>表名称列表</returns>
        Task<List<SchemaInfoDTO>> GetSchemaInfo();

        /// <summary>
        /// 获取表的列表
        /// </summary>
        /// <param name="tables">表名</param>
        /// <returns>表的列表集</returns>
        Task<List<SchemaInfoDTO>> GetSchemaFieldsInfo(string tables);

        /// <summary>
        /// 通过key获取SQL语句
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>SQL语句</returns>
        String GetSqlByKey(String key);
    }
}
