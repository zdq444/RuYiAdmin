//-----------------------------------------------------------------------
// <Copyright file="BizModuleService.cs" company="RuYiAdmin">
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// * Version : 4.0.30319.42000
// * Author  : auto generated by RuYiAdmin T4 Template
// * FileName: BizModuleService.cs
// * History : Created by RuYiAdmin 01/21/2022 13:40:04
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.BusinessModel.ModuleManagement;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.BusinessInterface.ModuleManagement;
using RuYiAdmin.Net.ServiceLayer.BaseService.Implementation;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.BusinessInterface.ModuleManagement;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.BusinessImplementation.ModuleManagement
{
    /// <summary>
    /// BizModule业务层实现
    /// </summary>
    public class BizModuleService : RuYiAdminBaseService<BizModule>, IBizModuleService
    {
        /// <summary>
        /// 仓储实例
        /// </summary>
        private readonly IBizModuleRepository BizModuleRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="BizModuleRepository"></param>
        public BizModuleService(IBizModuleRepository BizModuleRepository) : base(BizModuleRepository)
        {
            this.BizModuleRepository = BizModuleRepository;
        }
    }
}
