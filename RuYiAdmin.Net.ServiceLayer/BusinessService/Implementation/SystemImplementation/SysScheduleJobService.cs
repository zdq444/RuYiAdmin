﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Quartz;
using Quartz.Impl;
using RuYiAdmin.Net.CommonInfrastructure.Classes;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Exceptions.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface;
using RuYiAdmin.Net.ServiceLayer.BaseService.Implementation;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.SystemImplementation
{
    /// <summary>
    /// 计划任务业务层实现
    /// </summary>
    public class SysScheduleJobService : RuYiAdminBaseService<SysScheduleJob>, ISysScheduleJobService
    {
        #region 属性及构造函数

        /// <summary>
        /// 计划任务仓储实例
        /// </summary>
        private readonly ISysScheduleJobRepository ScheduleJobRepository;

        /// <summary>
        /// 计划任务
        /// </summary>
        private IScheduler Scheduler;

        /// <summary>
        /// Redis仓储实例
        /// </summary>
        private readonly IRedisRepository RedisRepository;

        /// <summary>
        /// 业务日志仓储接口实例
        /// </summary>
        private readonly ILoggerRepository LoggerRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="ScheduleJobRepository"></param>
        /// <param name="RedisRepository"></param>
        /// <param name="LoggerRepository"></param>
        public SysScheduleJobService(ISysScheduleJobRepository ScheduleJobRepository,
                                     IRedisRepository RedisRepository,
                                     ILoggerRepository LoggerRepository) : base(ScheduleJobRepository)
        {
            this.ScheduleJobRepository = ScheduleJobRepository;
            this.RedisRepository = RedisRepository;
            this.LoggerRepository = LoggerRepository;
        }

        #endregion

        #region 启动计划任务

        /// <summary>
        /// 启动计划任务
        /// </summary>
        /// <param name="jobId">任务编号</param>
        /// <param name="userId">操作人员编号</param>
        /// <returns></returns>
        public async Task StartScheduleJobAsync(Guid jobId, Guid userId)
        {
            try
            {
                var scheduleJobs = await this.RedisRepository.GetAsync<List<SysScheduleJob>>(RuYiGlobalConfig.SystemCacheConfig.ScheduleJobCacheName);
                SysScheduleJob scheduleJob = scheduleJobs.Where(t => t.Id == jobId).FirstOrDefault();

                if (scheduleJob != null)
                {
                    #region 预设运行时间

                    if (scheduleJob.StartTime == null)
                    {
                        scheduleJob.StartTime = DateTime.Now;
                    }
                    DateTimeOffset starRunTime = DateBuilder.NextGivenSecondDate(scheduleJob.StartTime, 1);

                    if (scheduleJob.EndTime == null)
                    {
                        scheduleJob.EndTime = DateTime.MaxValue.AddDays(-1);
                    }
                    DateTimeOffset endRunTime = DateBuilder.NextGivenSecondDate(scheduleJob.EndTime, 1);

                    #endregion

                    var jobName = scheduleJob.JobName;
                    var jobGroup = RuYiGlobalConfig.QuartzConfig.ScheduleJobGroup;
                    var jobTrigger = RuYiGlobalConfig.QuartzConfig.ScheduleJobTrigger + "/" + jobName;

                    var scheduler = await GetSchedulerAsync();

                    // 构建JobDetail
                    IJobDetail job = JobBuilder.Create(Type.GetType($"{scheduleJob.NameSpace}.{scheduleJob.JobImplement}"))
                                               .WithIdentity(jobName, jobGroup)
                                               .Build();
                    // 构建JobTrigger
                    ICronTrigger trigger = (ICronTrigger)TriggerBuilder.Create()
                                                                       .StartAt(starRunTime)
                                                                       .EndAt(endRunTime)
                                                                       .WithIdentity(jobTrigger, jobGroup)
                                                                       .WithCronSchedule(scheduleJob.CronExpression)
                                                                       .Build();

                    await scheduler.ScheduleJob(job, trigger);
                    await scheduler.Start();

                    //更新任务状态
                    scheduleJob.JobStatus = JobStatus.Running;

                    scheduleJob.Modifier = userId;
                    scheduleJob.ModifyTime = DateTime.Now;
                    scheduleJob.VersionId = Guid.NewGuid();

                    //更新状态
                    RuYiAdminDbScope.RuYiDbContext.Updateable(scheduleJob).ExecuteCommand();

                    #region 数据一致性维护

                    //删除旧数据
                    var old = scheduleJobs.Where(t => t.Id == scheduleJob.Id).FirstOrDefault();
                    scheduleJobs.Remove(old);

                    //添加新数据
                    scheduleJobs.Add(scheduleJob);

                    //回写缓存
                    await this.RedisRepository.SetAsync(RuYiGlobalConfig.SystemCacheConfig.ScheduleJobCacheName, scheduleJobs, -1);

                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw new RuYiAdminCustomException(ex.Message);
            }
        }

        #endregion

        #region 暂停计划任务

        /// <summary>
        /// 暂停计划任务
        /// </summary>
        /// <param name="jobId">作业编号</param>
        /// <param name="userId">操作人员编号</param>
        /// <returns></returns>
        public async Task PauseScheduleJobAsync(Guid jobId, Guid userId)
        {
            try
            {
                var scheduleJobs = await this.RedisRepository.GetAsync<List<SysScheduleJob>>(RuYiGlobalConfig.SystemCacheConfig.ScheduleJobCacheName);
                SysScheduleJob scheduleJob = scheduleJobs.Where(t => t.Id == jobId).FirstOrDefault();

                if (scheduleJob != null)
                {
                    var jobName = scheduleJob.JobName;
                    var jobGroup = RuYiGlobalConfig.QuartzConfig.ScheduleJobGroup;

                    var scheduler = await GetSchedulerAsync();
                    var jobKey = new JobKey(jobName, jobGroup);

                    if (await scheduler.CheckExists(jobKey))
                    {
                        //任务暂停
                        await scheduler.PauseJob(jobKey);
                    }

                    scheduleJob.JobStatus = JobStatus.Stopped;

                    scheduleJob.Modifier = userId;
                    scheduleJob.ModifyTime = DateTime.Now;
                    scheduleJob.VersionId = Guid.NewGuid();

                    //更新状态
                    RuYiAdminDbScope.RuYiDbContext.Updateable(scheduleJob).ExecuteCommand();

                    #region 数据一致性维护

                    //删除旧数据
                    var old = scheduleJobs.Where(t => t.Id == scheduleJob.Id).FirstOrDefault();
                    scheduleJobs.Remove(old);

                    //添加新数据
                    scheduleJobs.Add(scheduleJob);

                    //回写缓存
                    await this.RedisRepository.SetAsync(RuYiGlobalConfig.SystemCacheConfig.ScheduleJobCacheName, scheduleJobs, -1);

                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw new RuYiAdminCustomException(ex.Message);
            }
        }

        #endregion

        #region 恢复计划任务

        /// <summary>
        /// 恢复计划任务
        /// </summary>
        /// <param name="jobId">作业编号</param>
        /// <param name="userId">操作人员编号</param>
        /// <returns></returns>
        public async Task ResumeScheduleJobAsync(Guid jobId, Guid userId)
        {
            try
            {
                var scheduleJobs = await this.RedisRepository.GetAsync<List<SysScheduleJob>>(RuYiGlobalConfig.SystemCacheConfig.ScheduleJobCacheName);
                SysScheduleJob scheduleJob = scheduleJobs.Where(t => t.Id == jobId).FirstOrDefault();

                if (scheduleJob != null)
                {
                    var jobName = scheduleJob.JobName;
                    var jobGroup = RuYiGlobalConfig.QuartzConfig.ScheduleJobGroup;

                    var scheduler = await GetSchedulerAsync();
                    var jobKey = new JobKey(jobName, jobGroup);

                    if (!await scheduler.CheckExists(jobKey))
                    {
                        await StartScheduleJobAsync(jobId, userId);
                    }
                    //恢复
                    await scheduler.ResumeJob(jobKey);

                    scheduleJob.JobStatus = JobStatus.Running;

                    scheduleJob.Modifier = userId;
                    scheduleJob.ModifyTime = DateTime.Now;
                    scheduleJob.VersionId = Guid.NewGuid();

                    //更新状态
                    RuYiAdminDbScope.RuYiDbContext.Updateable(scheduleJob).ExecuteCommand();

                    #region 数据一致性维护

                    //删除旧数据
                    var old = scheduleJobs.Where(t => t.Id == scheduleJob.Id).FirstOrDefault();
                    scheduleJobs.Remove(old);

                    //添加新数据
                    scheduleJobs.Add(scheduleJob);

                    //回写缓存
                    await this.RedisRepository.SetAsync(RuYiGlobalConfig.SystemCacheConfig.ScheduleJobCacheName, scheduleJobs, -1);

                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw new RuYiAdminCustomException(ex.Message);
            }
        }

        #endregion

        #region 删除计划任务

        /// <summary>
        /// 删除计划任务
        /// </summary>
        /// <param name="jobId">作业编号</param>
        /// <param name="userId">操作人员编号</param>
        /// <returns></returns>
        public async Task DeleteScheduleJobAsync(Guid jobId, Guid userId)
        {
            try
            {
                var scheduleJobs = await this.RedisRepository.GetAsync<List<SysScheduleJob>>(RuYiGlobalConfig.SystemCacheConfig.ScheduleJobCacheName);
                SysScheduleJob scheduleJob = scheduleJobs.Where(t => t.Id == jobId).FirstOrDefault();

                if (scheduleJob != null)
                {
                    var jobName = scheduleJob.JobName;
                    var jobGroup = RuYiGlobalConfig.QuartzConfig.ScheduleJobGroup;

                    var scheduler = await GetSchedulerAsync();
                    var jobKey = new JobKey(jobName, jobGroup);

                    if (await scheduler.CheckExists(jobKey))
                    {
                        await scheduler.PauseJob(jobKey);
                        await scheduler.DeleteJob(jobKey);
                    }

                    scheduleJob.JobStatus = JobStatus.Stopped;

                    scheduleJob.IsDel = (int)DeletionType.Deleted;
                    scheduleJob.Modifier = userId;
                    scheduleJob.ModifyTime = DateTime.Now;
                    scheduleJob.VersionId = Guid.NewGuid();

                    //逻辑删除
                    RuYiAdminDbScope.RuYiDbContext.Updateable(scheduleJob).ExecuteCommand();

                    #region 数据一致性维护

                    //删除旧数据
                    var old = scheduleJobs.Where(t => t.Id == scheduleJob.Id).FirstOrDefault();
                    scheduleJobs.Remove(old);

                    //回写缓存
                    await this.RedisRepository.SetAsync(RuYiGlobalConfig.SystemCacheConfig.ScheduleJobCacheName, scheduleJobs, -1);

                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw new RuYiAdminCustomException(ex.Message);
            }
        }

        #endregion

        #region 启动业务作业

        /// <summary>
        /// 启动业务作业
        /// </summary>
        /// <returns></returns>
        public async Task StartScheduleJobAsync()
        {
            var scheduleJobs = await this.RedisRepository.GetAsync<List<SysScheduleJob>>(RuYiGlobalConfig.SystemCacheConfig.ScheduleJobCacheName);

            //支持集群作业
            if (RuYiGlobalConfig.QuartzConfig.SupportGroup)
            {
                //仅加载本节点定时任务
                scheduleJobs = scheduleJobs.Where(t => t.GroupId == RuYiGlobalConfig.QuartzConfig.GroupId).ToList();
            }

            foreach (var item in scheduleJobs)
            {
                if (item.JobStatus.Equals(JobStatus.Running))
                {
                    #region 预设时间

                    if (item.StartTime == null)
                    {
                        item.StartTime = DateTime.Now;
                    }
                    DateTimeOffset starRunTime = DateBuilder.NextGivenSecondDate(item.StartTime, 1);

                    if (item.EndTime == null)
                    {
                        item.EndTime = DateTime.MaxValue.AddDays(-1);
                    }
                    DateTimeOffset endRunTime = DateBuilder.NextGivenSecondDate(item.EndTime, 1);

                    #endregion

                    var jobName = item.JobName;
                    var jobGroup = RuYiGlobalConfig.QuartzConfig.ScheduleJobGroup;
                    var jobTrigger = RuYiGlobalConfig.QuartzConfig.ScheduleJobTrigger + "/" + jobName;

                    var schedf = new StdSchedulerFactory();
                    var scheduler = await schedf.GetScheduler();

                    IJobDetail job = JobBuilder.Create(Type.GetType($"{item.NameSpace}.{item.JobImplement}"))
                      .WithIdentity(jobName, jobGroup)
                      .Build();
                    ICronTrigger trigger = (ICronTrigger)TriggerBuilder.Create()
                                                 .StartAt(starRunTime)
                                                 .EndAt(endRunTime)
                                                 .WithIdentity(jobTrigger, jobGroup)
                                                 .WithCronSchedule(item.CronExpression)
                                                 .Build();

                    await scheduler.ScheduleJob(job, trigger);
                    await scheduler.Start();
                }
            }
        }

        #endregion

        #region 加载计划任务缓存

        /// <summary>
        /// 加载计划任务缓存
        /// </summary>
        public async Task LoadBusinessScheduleJobCache()
        {
            int totalCount = 0;
            var scheduleJobs = await this.ScheduleJobRepository.DefaultSqlQueryAsync(new SearchCondition(), totalCount);
            await this.RedisRepository.SetAsync(RuYiGlobalConfig.SystemCacheConfig.ScheduleJobCacheName, scheduleJobs, -1);

            this.LoggerRepository.Info("系统计划任务缓存加载完成");
        }

        #endregion

        #region 清理计划任务缓存

        /// <summary>
        /// 清理计划任务缓存
        /// </summary>
        public async Task ClearBusinessScheduleJobCache()
        {
            await this.RedisRepository.DeleteAsync(new string[] { RuYiGlobalConfig.SystemCacheConfig.ScheduleJobCacheName });

            this.LoggerRepository.Info("系统计划任务缓存清理完成");
        }

        #endregion

        #region 获取任务对象

        /// <summary>
        /// 获取任务对象
        /// </summary>
        /// <returns>任务对象</returns>
        private async Task<IScheduler> GetSchedulerAsync()
        {
            if (this.Scheduler != null)
            {
                return this.Scheduler;
            }
            else
            {
                ISchedulerFactory schedf = new StdSchedulerFactory();
                this.Scheduler = await schedf.GetScheduler();
                return this.Scheduler;
            }
        }

        #endregion
    }
}
