﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface;
using RuYiAdmin.Net.ServiceLayer.BaseService.Implementation;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using System.Linq;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.SystemImplementation
{
    /// <summary>
    /// 菜单多语业务层实现
    /// </summary>
    public class SysMenuLanguageService : RuYiAdminBaseService<SysMenuLanguage>, ISysMenuLanguageService
    {
        #region 属性及构造函数

        /// <summary>
        /// 菜单多语仓储实例
        /// </summary>
        private readonly ISysMenuLanguageRepository MenuLanguageRepository;

        /// <summary>
        /// Redis仓储实例
        /// </summary>
        private readonly IRedisRepository RedisRepository;

        /// <summary>
        /// 业务日志仓储接口实例
        /// </summary>
        private readonly ILoggerRepository LoggerRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="MenuLanguageRepository"></param>
        /// <param name="RedisRepository"></param>
        /// <param name="LoggerRepository"></param>
        public SysMenuLanguageService(ISysMenuLanguageRepository MenuLanguageRepository,
                                      IRedisRepository RedisRepository,
                                      ILoggerRepository LoggerRepository) : base(MenuLanguageRepository)
        {
            this.MenuLanguageRepository = MenuLanguageRepository;
            this.RedisRepository = RedisRepository;
            this.LoggerRepository = LoggerRepository;
        }

        #endregion

        #region 服务层公有方法

        #region 加载菜单与多语缓存

        /// <summary>
        /// 加载菜单与多语缓存
        /// </summary>
        public async Task LoadSystemMenuLanguageCache()
        {
            int totalCount = 0;
            var menuLanguages = (await this.MenuLanguageRepository.DefaultSqlQueryAsync(new SearchCondition(), totalCount)).ToList();
            await this.RedisRepository.SetAsync(RuYiGlobalConfig.SystemCacheConfig.MenuAndLanguageCacheName, menuLanguages, -1);

            this.LoggerRepository.Info("系统菜单与多语缓存加载完成");
        }

        #endregion

        #region 清理菜单与多语缓存

        /// <summary>
        /// 清理菜单与多语缓存
        /// </summary>
        public async Task ClearSystemMenuLanguageCache()
        {
            await this.RedisRepository.DeleteAsync(new string[] { RuYiGlobalConfig.SystemCacheConfig.MenuAndLanguageCacheName });

            this.LoggerRepository.Info("系统菜单与多语缓存清理完成");
        }

        #endregion

        #endregion
    }
}