﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface;
using RuYiAdmin.Net.ServiceLayer.BaseService.Implementation;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using System.Linq;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.SystemImplementation
{
    /// <summary>
    /// 多语业务层实现
    /// </summary>
    public class SysLanguageService : RuYiAdminBaseService<SysLanguage>, ISysLanguageService
    {
        #region 属性及构造函数

        /// <summary>
        /// 多语仓储实例
        /// </summary>
        private readonly ISysLanguageRepository LanguageRepository;

        /// <summary>
        /// Redis仓储实例
        /// </summary>
        private readonly IRedisRepository RedisRepository;

        /// <summary>
        /// 业务日志仓储接口实例
        /// </summary>
        private readonly ILoggerRepository LoggerRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="LanguageRepository"></param>
        /// <param name="RedisRepository"></param>
        /// <param name="LoggerRepository"></param>
        public SysLanguageService(ISysLanguageRepository LanguageRepository,
                                  IRedisRepository RedisRepository,
                                  ILoggerRepository LoggerRepository) : base(LanguageRepository)
        {
            this.LanguageRepository = LanguageRepository;
            this.RedisRepository = RedisRepository;
            this.LoggerRepository = LoggerRepository;
        }

        #endregion

        #region 加载系统多语缓存

        /// <summary>
        /// 加载系统多语缓存
        /// </summary>
        public async Task LoadSystemLanguageCache()
        {
            int totalCount = 0;
            var languages = (await this.LanguageRepository.DefaultSqlQueryAsync(new SearchCondition(), totalCount)).
                                                    OrderBy(t => t.OrderNumber).ToList();
            await this.RedisRepository.SetAsync(RuYiGlobalConfig.SystemCacheConfig.LanguageCacheName, languages, -1);

            this.LoggerRepository.Info("系统多语缓存加载完成");
        }

        #endregion

        #region 清理系统多语缓存

        /// <summary>
        /// 清理系统多语缓存
        /// </summary>
        public async Task ClearSystemLanguageCache()
        {
            await this.RedisRepository.DeleteAsync(new string[] { RuYiGlobalConfig.SystemCacheConfig.LanguageCacheName });

            this.LoggerRepository.Info("系统多语缓存清理完成");
        }

        #endregion
    }
}