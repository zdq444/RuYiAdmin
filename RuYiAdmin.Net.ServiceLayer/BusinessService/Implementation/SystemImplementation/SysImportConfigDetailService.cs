﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface;
using RuYiAdmin.Net.ServiceLayer.BaseService.Implementation;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.SystemImplementation
{
    /// <summary>
    /// 导入配置明细业务层实现
    /// </summary>
    public class SysImportConfigDetailService : RuYiAdminBaseService<SysImportConfigDetail>, ISysImportConfigDetailService
    {
        /// <summary>
        /// 导入配置明细访问层实例
        /// </summary>
        private readonly ISysImportConfigDetailRepository ImportConfigDetailRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="ImportConfigDetailRepository"></param>
        public SysImportConfigDetailService(ISysImportConfigDetailRepository ImportConfigDetailRepository) : base(ImportConfigDetailRepository)
        {
            this.ImportConfigDetailRepository = ImportConfigDetailRepository;
        }
    }
}
