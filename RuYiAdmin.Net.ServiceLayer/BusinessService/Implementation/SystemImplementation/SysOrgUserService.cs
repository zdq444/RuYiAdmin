﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface;
using RuYiAdmin.Net.ServiceLayer.BaseService.Implementation;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.SystemImplementation
{
    /// <summary>
    /// 机构用户业务层实现
    /// </summary>
    public class SysOrgUserService : RuYiAdminBaseService<SysOrgUser>, ISysOrgUserService
    {
        /// <summary>
        /// 机构与用户仓储实例
        /// </summary>
        private readonly ISysOrgUserRepository OrgUserRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="OrgUserRepository"></param>
        public SysOrgUserService(ISysOrgUserRepository OrgUserRepository) : base(OrgUserRepository)
        {
            this.OrgUserRepository = OrgUserRepository;
        }
    }
}
