﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface;
using RuYiAdmin.Net.ServiceLayer.BaseService.Implementation;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using System.Linq;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.SystemImplementation
{
    /// <summary>
    /// 角色菜单业务层实现
    /// </summary>
    class SysRoleMenuService : RuYiAdminBaseService<SysRoleMenu>, ISysRoleMenuService
    {
        #region 属性及构造函数

        /// <summary>
        /// 角色与菜单仓储实例
        /// </summary>
        private readonly ISysRoleMenuRepository RoleMenuRepository;

        /// <summary>
        /// Redis仓储实例
        /// </summary>
        private readonly IRedisRepository RedisRepository;

        /// <summary>
        /// 业务日志仓储接口实例
        /// </summary>
        private readonly ILoggerRepository LoggerRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="RoleMenuRepository"></param>
        /// <param name="RedisRepository"></param>
        /// <param name="LoggerRepository"></param>
        public SysRoleMenuService(ISysRoleMenuRepository RoleMenuRepository,
                                  IRedisRepository RedisRepository,
                                  ILoggerRepository LoggerRepository) : base(RoleMenuRepository)
        {
            this.RoleMenuRepository = RoleMenuRepository;
            this.RedisRepository = RedisRepository;
            this.LoggerRepository = LoggerRepository;
        }

        #endregion

        #region 服务层公有方法

        #region 加载角色与菜单缓存

        /// <summary>
        /// 加载角色与菜单缓存
        /// </summary>
        public async Task LoadSystemRoleMenuCache()
        {
            int totalCount = 0;
            var roleMenus = (await this.RoleMenuRepository.DefaultSqlQueryAsync(new SearchCondition(), totalCount)).ToList();
            await this.RedisRepository.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleAndMenuCacheName, roleMenus, -1);

            this.LoggerRepository.Info("系统角色与菜单缓存加载完成");
        }

        #endregion

        #region 清理角色与菜单缓存

        /// <summary>
        /// 清理角色与菜单缓存
        /// </summary>
        public async Task ClearSystemRoleMenuCache()
        {
            await this.RedisRepository.DeleteAsync(new string[] { RuYiGlobalConfig.SystemCacheConfig.RoleAndMenuCacheName });

            this.LoggerRepository.Info("系统角色与菜单缓存清理完成");
        }

        #endregion

        #endregion
    }
}
