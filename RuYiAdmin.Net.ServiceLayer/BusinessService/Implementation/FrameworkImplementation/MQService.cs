﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.FrameworkInterface;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.FrameworkImplementation
{
    /// <summary>
    /// ActiveMQ服务
    /// </summary>
    public class MQService : IMQService
    {
        /// <summary>
        /// ActiveMQ访问层实例
        /// </summary>
        private readonly IMQRepository MQRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="MQRepository">管道实例</param>
        public MQService(IMQRepository MQRepository)
        {
            this.MQRepository = MQRepository;
        }

        /// <summary>
        /// 发送Topic
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="topicName">topic别名</param>
        /// <returns></returns>
        public void SendTopic(string message, string topicName = null)
        {
            this.MQRepository.SendTopic(HttpUtility.UrlEncode(message, Encoding.UTF8), topicName);
        }

        /// <summary>
        /// 发送Topic
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="topicName">topic别名</param>
        /// <returns></returns>
        public async Task SendTopicAsync(string message, string topicName = null)
        {
            await this.MQRepository.SendTopicAsync(HttpUtility.UrlEncode(message, Encoding.UTF8), topicName);
        }

        /// <summary>
        /// 发送Queue
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="queueName">queue别名</param>
        /// <returns></returns>
        public void SendQueue(string message, string queueName = null)
        {
            this.MQRepository.SendQueue(HttpUtility.UrlEncode(message, Encoding.UTF8), queueName);
        }

        /// <summary>
        /// 发送Queue
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="queueName">queue别名</param>
        /// <returns></returns>
        public async Task SendQueueAsync(string message, string queueName = null)
        {
            await this.MQRepository.SendQueueAsync(HttpUtility.UrlEncode(message, Encoding.UTF8), queueName);
        }
    }
}
