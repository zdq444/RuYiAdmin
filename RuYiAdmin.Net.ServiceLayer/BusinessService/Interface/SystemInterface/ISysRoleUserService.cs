﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BaseService.Interface;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface
{
    /// <summary>
    /// 角色用户业务层接口
    /// </summary>
    public interface ISysRoleUserService : IRuYiAdminBaseService<SysRoleUser>
    {
        /// <summary>
        /// 加载角色与用户缓存
        /// </summary>
        Task LoadSystemRoleUserCache();

        /// <summary>
        /// 清理角色与用户缓存
        /// </summary>
        Task ClearSystemRoleUserCache();
    }
}
