﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BaseService.Interface;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface
{
    /// <summary>
    /// 数据字典业务层接口
    /// </summary>
    public interface ISysCodeTableService : IRuYiAdminBaseService<SysCodeTable>
    {
        /// <summary>
        /// 获取字典树
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        Task<QueryResponseResult<SysCodeTableDTO>> GetCodeTreeNodes();

        /// <summary>
        /// 加载数据字典缓存
        /// </summary>
        Task LoadSystemCodeTableCache();

        /// <summary>
        /// 清理数据字典缓存
        /// </summary>
        Task ClearSystemCodeTableCache();
    }
}
