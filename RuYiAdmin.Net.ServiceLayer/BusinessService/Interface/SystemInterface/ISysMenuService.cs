﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BaseService.Interface;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface
{
    /// <summary>
    /// 菜单业务层接口
    /// </summary>
    public interface ISysMenuService : IRuYiAdminBaseService<SysMenu>
    {
        /// <summary>
        /// 获取菜单树
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        Task<QueryResponseResult<SysMenuDTO>> GetMenuTreeNodes();

        /// <summary>
        /// 加载系统菜单缓存
        /// </summary>
        Task LoadSystemMenuCache();

        /// <summary>
        /// 清理系统菜单缓存
        /// </summary>
        Task ClearSystemMenuCache();
    }
}
