﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BaseService.Interface;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface
{
    /// <summary>
    /// 菜单多语业务层接口
    /// </summary>
    public interface ISysMenuLanguageService : IRuYiAdminBaseService<SysMenuLanguage>
    {
        /// <summary>
        /// 加载菜单与多语缓存
        /// </summary>
        Task LoadSystemMenuLanguageCache();

        /// <summary>
        /// 清理菜单与多语缓存
        /// </summary>
        Task ClearSystemMenuLanguageCache();
    }
}
