# RuYiAdmin

#### 鸣谢
本项目所采用WebStorm和IntelliJ IDEA商业版由[JetBrains](https://jb.gg/OpenSourceSupport)提供。
![JetBrains Logo (Main) logo](https://resources.jetbrains.com/storage/products/company/brand/logos/jb_beam.png)

#### 项目简介
```
如意Admin是一款前后端分离的、代码能够自动生成的、基于.NET8的、可以跨平台的、低代
码、分布式、多线程、高性能、支持4A认证、满足等保三要求的企业级WEB RBAC统一安全管
理平台解决方案。

RuYiAdmin支持智慧大屏幕。可一键生成多个业务的视图层、控制层、服务层、仓储层、领域
层和DTO业务模型层代码。集成VForm表单设计器，支持PC、Pad、手机H5页面表单设计与预览。
ORM采用SqlSugar，关系库支持MySql、SqlServer、Oracle、PostgreSql、OpenGauss
数据库，支持多数据源。非关系库支持Redis、MongoDB、Elasticsearch、Meilisearch。
消息总线支持ActiveMQ、RabbitMQ、Kafka、Redis。企业级配置中心支持携程Apollo。
支持统一异常处理，支持RateLimit分布式限流。支持服务发现与健康检查，支持熔断与降级。
支持MiniProfiler接口性能分析。支持分布式事务解决方案CAP和事件总线EventBus。支持分布
式定时任务的管理与调度。支持FTP、FXP、FTPS、SFTP。防止SQL注入，防止Token劫持与接口
渗透，接口具备幂等性、防止请求重复提交。支持国产SM加密算法。gateway网关支持ocelot。
全链路支持https加密传输协议。
```

#### 学习交流
```
如意Admin视野宽广，功能高级，生态全面。学习、使用本项目，让你站在巨人的肩膀上。为把
Java拉下神坛贡献一份力量，本项目自.NET8起开源免费。企业版学习交流群：806522770、
664802523。
注：
1.项目星星数量为自然流量，绝无虚假。项目突破一千星后做深度优化与更新。
2.SpringBoot地址：https://gitee.com/pang-mingjun/RuYiAdmin-SpringBoot
3.SpringCloud地址：https://gitee.com/pang-mingjun/RuYiAdmin-SpringCloud

```

#### 软件架构
![输入图片说明](images/1650446074(1).jpg)


#### 项目优势

```
前端框架	
	使用Vue2，UI采用Element UI，框架使用Vue Element Admin
	对于通用API做了统一性的封装
	对于BasePage界面与通用方法做了较为全面的封装
	支持锁屏、水印
	支持按钮、视图的可见性控制
	支持按钮、视图级别的颗粒授权
	支持权限下放
	支持智慧大屏幕
	支持视图层业务多语
	支持VForm表单设计器，支持PC、Pad、手机H5页面表单设计与预览
	支持敏感数据通信加密
后端框架	
	采用.NET8、支持跨平台
	支持Linux Docker
	支持系统集成、支持统一认证（非同源用户群组）
	采用ASP.NET Core Web API，既可用于构建独立服务，也可以用于支撑前端业务
	使用Swagger作为Web API的管理工具，为接口与Model做了精细的注释
	支持API路由白名单、支持Request Headers验证自由配置
	使用JwtSecurity验证，防止Web API滥调
	封装了统一的数据返回格式
	封装了强大的查询条件，支持前端的自定义查询	
	支持菜单多语
	支持一键生成多个业务的视图层、控制层、服务层、仓储层、领域层代码
	支持在线预览系统帮助
	支持配置信息热加载、热更新
	支持用户行为的AOP自动监控
	支持用户行为的AOP自动鉴权
	使用AutoFac，实现依赖自动注入
	支持log4net，同时支持控制台日志输出
	支持Quartz作业与分布式定时任务的管理与调度
	ORM采用SqlSugar，支持多种数据库、支持多租户
	实现了灵活高效的SQL访问底层，支持SQL热加载、热更新, 支持SQL与代码分离
	支持一主两从的读写分离
	支持乐观锁并发控制
	支持大数据写入、更新
	支持海量数据写入、更新
	支持数据的逻辑与物理删除
	封装了统一的基类模型、支持个性化Excel导出，支持Excel导入常规校验
	支持相对路径、绝对路径或NAS存储
	支持AutoMapper，实现POCO与DTO自动转化
	支持实时消息
	支持禁用用户实时踢出系统
	支持线程池、多任务
	支持服务熔断与降级
	支持熔断邮件告警
	支持Consul服务治理与健康检查
	支持并发限制
	支持RateLimit分布式限流
	支持统一异常处理
	防止SQL注入
	防止Token劫持与接口渗透
	接口幂等性、防止请求重复提交。
	支持OCR
	支持MongoDB
	支持ES全文检索
	支持Apollo配置中心
	支持接口性能分析
	支持分布式事务Cap和EventBus
	支持FTP、FXP、FTPS、SFTP
	支持国产SM加密算法及国际通用Rsa与Aes高级加密算法
	Gateway网关支持ocelot
	全链路支持https加密传输协议
数据库	
	构建了可视化的ER关系模型
	关系库支持MySql、SqlServer、SQLite、Oracle、PostgreSQL、OpenGauss数据库
	提供一键执行的数据库初始化脚本
	支持自动构建数据库
	非关系型数据库支持Redis、MongoDB、Elasticsearch、Meilisearch
中间件	
	支持Redis高速缓存，支持哨兵模式集群
	集成了ActiveMQ，支持Master Slave和Broker Cluster结合的MQ集群
	消息总线支持ActiveMQ、RabbitMQ、Kafka、Redis
```

系统设计思想
![输入图片说明](images/1645595966(1).jpg)

#### 安装教程

```
1.安装Redis。
2.安装ActiveMQ,支持RabbitMQ。
3.安装Mysql数据库，支持一主两从、读写分离。作者使用的是mysql8，如果搭建基于mycat
的读写分离集群，建议使用mysql5.7。
支持自动构建数据库，修改AutomaticallyBuildDatabase的值为true，修改SqlScript
Path脚本路径，系统启动后将自动构建数据库。
系统同时支持SqlServer(2019)、Oracle(19c)、PostgreSQL(14)、SQLite数据库。
审计日志默认进入非关系库MongoDB（日志进入非关系库需要安装MongoDB），同时支持
Elasticsearch（全文检索）。
4.安装NodeJs。
5.克隆代码。
6.初始化数据库。结构与数据位于webapi/RuYiAdminData/DataModel、DataStructrue下。
如果使用低版本mysql，可能需要修改数据库初始化脚本。
7.修改配置。修改后端appsettings.json中Redis、ActiveMQ、Mysql、MongoDB数据库连
接串。前端mq配置位于src/constants/active-mq.js。
8.系统前端。前端管理目录位于webvue/wwwroot。推荐使用Visual Studio Code开源软件
编辑。前端的使用可以参看Vue Element Admin官网。
9.系统后端。后端采用Visual Studio 2022开发工具，请使用最新版本。右键解决方案，设
置多个启动项目，同时启动Gateway和WebApi。
```

#### 系统截图
1. 星空粒子登录页（默认密码：123456）
![输入图片说明](images/11111.png)
![输入图片说明](images/denglu.jpg)
1. 系统首页
![输入图片说明](images/3333.png)
![输入图片说明](images/1657429883534.jpg)
![输入图片说明](images/1657429935025.jpg)
1. 机构管理
![输入图片说明](images/1657430065851.jpg)
1. 用户管理
![输入图片说明](images/1657430137931.jpg)
1. 菜单管理
![输入图片说明](images/1657430164105.jpg)
1. 角色管理
![输入图片说明](images/1657430190368.jpg)
1. 数据字典
![输入图片说明](images/1657430217440.jpg)
1. 审计日志
![输入图片说明](images/1657430249259.jpg)
![输入图片说明](images/1657430283777.jpg)
1. 导入配置及数据导入合法性校验
![输入图片说明](images/1657430305540.jpg)
![输入图片说明](images/1657430320485.jpg)
![输入图片说明](https://images.gitee.com/uploads/images/2021/0602/170515_26e80400_8819418.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0602/170401_4b597be0_8819418.png "屏幕截图.png")
1. 在线任务调度
![输入图片说明](images/1657430389298.jpg)
1. 系统多语管理
![输入图片说明](images/1657430410128.jpg)
1. 行政区域管理
![输入图片说明](images/1657430444850.jpg)
1. 通知公告管理
![输入图片说明](images/1657430470796.jpg)
![输入图片说明](images/1657430507536.jpg)
![输入图片说明](images/1657430539650.jpg)
![输入图片说明](images/1657430557654.jpg)

1. 在线用户管理
![输入图片说明](images/1657431300325.jpg)
1. 服务器监控
![输入图片说明](images/1657431346248.jpg)
1. 系统文件统计
![输入图片说明](images/wjtj.jpg)
1. 接口性能分析
![输入图片说明](images/jkxnfx.jpg)

1. 系统手册
![输入图片说明](images/1657431381705.jpg)

1. 表单设计器
![输入图片说明](images/1657431440037.jpg)
1. 代码生成器
![输入图片说明](images/1657431467054.jpg)
![输入图片说明](images/1657432107996.jpg)
![输入图片说明](images/%7DWY66F83%5BJ5S6WUA~%7DP%5DV2S.png)
1. 系统WebAPI
![输入图片说明](images/1657431493528.jpg)
1. 系统数据库设计文档
![输入图片说明](images/sjksjwd.jpg)

1. 智慧大屏幕
![输入图片说明](images/QQ%E5%9B%BE%E7%89%8720220613135250.png)


1. 系统集成及统一认证
- 系统集成
![输入图片说明](images/1657432336222.jpg)
- 统一授权
![输入图片说明](images/1657432366108.jpg)
- 一对多授权
![输入图片说明](images/yddsq.jpg)
- 统一认证
http://localhost:9527/#/appLogin
![输入图片说明](images/1657432445112.jpg)
- 统一访问
![输入图片说明](images/jgg.png)

1. swagger
![输入图片说明](images/1657431528530.jpg)
![输入图片说明](https://images.gitee.com/uploads/images/2021/0429/115235_fd32f272_8819418.png "屏幕截图.png")

1. 其他

- 极简代码风格示例

```
    /// <summary>
    /// 日志业务层接口
    /// </summary>
    public interface ILogService : IBaseService<SysLog>
    {
    }
```

#### 后话

```
   我看过很多.Net的开源框架，无论是前端，还是后端或者框架业务的完备性，均没有一个
令我满意的框架。在使用了Vue版本的若依Spring Boot（Java框架）之后，我发现她很全
面、很强大，但是也存在一些问题：那就是前端与后端均没有做统一的抽象与封装。
   架构如果没有层次、没有封装，那就意味着代码量会很大。于是我有了前端、控制器、服
务层、仓储层及通用业务做统一、泛型抽象与封装的打算，以此来最大限度的减少业务层代码
、减轻开发工作量。
   2021年3月，在查阅了很多优秀的开源项目之后，我初步完成了框架的设计思想和技术栈
的选型。2021年4月，我开始了框架底层的研发。随后一鼓作气将其在工作之余完成，同时将
她开源。为了纪念她的诞生，我用我闺女的小名如意来为她命名，称之为“如意Admin”
（项目名称为RuYiAdmin）。截至2022年2月18号，RuYiAdmin开源项目已历经15个大小
版本的迭代，架构趋于稳定、业务趋于成熟。
   “桃李春风一杯酒，江湖夜雨十年灯”。 RuYiAdmin开源项目是我近十年框架业务的总结
与技术的沉淀。我有意将RuYiAdmin开源项目与目前流行的技术结合起来，目的是想将我个人
积累的经验与技术转化为强有力的生产工具，以此来帮助更多的人。
   最后，我以个人最崇高的敬意，衷心地感谢那些无私的开源社区贡献者为全人类IT信息的
进步所做出的伟大贡献，是你们的付出感召着我们、照亮着我们前进的道路！

```