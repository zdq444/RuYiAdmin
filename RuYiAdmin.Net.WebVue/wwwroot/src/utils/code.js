import { RuYiAdmin } from '@/api/base-api'

export function getCode(code) {
  const url = 'SysCodeTableManagement/GetChildrenByCode/' + code
  return RuYiAdmin.Get(url, null)
}
