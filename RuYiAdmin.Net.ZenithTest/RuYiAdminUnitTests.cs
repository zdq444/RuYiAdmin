using KYSharp.SM;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils;

namespace RuYiAdmin.Net.ZenithTest
{
    public class RuYiAdminUnitTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void RuYiAdminTest()
        {
            Console.WriteLine("RuYiAdmin Test Passed!");
            Assert.Pass();
        }

        [Test]
        public void Hash256Test()
        {
            var sha = RuYiHashUtil.HashToHexString256("RuYiAdmin");
            Console.WriteLine(sha);
            Console.WriteLine(sha.Length);
        }

        [Test]
        public void Hash512Test()
        {
            var sha = RuYiHashUtil.HashToHexString512("RuYiAdmin");
            Console.WriteLine(sha);
            Console.WriteLine(sha.Length);
        }

        [Test]
        public void SM2Test()
        {
            String text = "RuYiAdmin";
            String publickey = "";//公钥            
            String privatekey = "";//私钥

            SM2Utils.GenerateKeyPair(out publickey, out privatekey);//生成公钥和私钥对
            Console.WriteLine("publickey：" + publickey);
            Console.WriteLine("privatekey：" + privatekey);

            Console.WriteLine("加密明文: " + text);

            //开始加密
            string cipherText = SM2Utils.Encrypt_Hex(publickey, text, System.Text.Encoding.UTF8);
            Console.WriteLine("密文: " + cipherText);

            //开始解密
            string plainText = SM2Utils.Decrypt_Hex(privatekey, cipherText, System.Text.Encoding.UTF8);
            Console.WriteLine("明文: " + plainText);
        }

        [Test]
        public void SM4Test()
        {
            String plainText = "RuYiAdmin";

            SM4Utils sm4 = new SM4Utils();
            sm4.secretKey = "JeF8U9wHFOMfs2Y8";
            sm4.hexString = false;

            Console.WriteLine("ECB模式");
            String cipherText = sm4.Encrypt_ECB(plainText);
            Console.WriteLine("密文: " + cipherText);

            plainText = sm4.Decrypt_ECB(cipherText);
            Console.WriteLine("明文: " + plainText);

            Console.WriteLine("CBC模式");
            sm4.iv = "UISwD9fW6cFh9SNS";
            cipherText = sm4.Encrypt_CBC(plainText);
            Console.WriteLine("密文: " + cipherText);

            plainText = sm4.Decrypt_CBC(cipherText);
            Console.WriteLine("明文: " + plainText);
        }
    }
}