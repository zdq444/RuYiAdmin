﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Nest;
using Newtonsoft.Json;
using RuYiAdmin.Net.CommonInfrastructure.Constants.System;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Exceptions.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Extensions.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;

namespace RuYiAdmin.Net.CommonInfrastructure.Models
{
    /// <summary>
    /// 查询条件
    /// </summary>
    public class SearchCondition
    {
        /// <summary>
        /// 起始页
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 分页数量
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public String Sort { get; set; }

        /// <summary>
        /// 查询项
        /// </summary>
        public List<SearchItem> SearchItems { get; set; }

        /// <summary>
        /// 无参构造函数
        /// </summary>
        public SearchCondition()
        {

        }

        /// <summary>
        /// 有参构造函数
        /// </summary>
        /// <param name="pageIndex">起始页</param>
        /// <param name="pageSize">分页数量</param>
        /// <param name="sort">排序字段</param>
        /// <param name="searchItems">查询条件</param>
        public SearchCondition(int pageIndex, int pageSize, String sort, List<SearchItem> searchItems)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
            Sort = sort;
            SearchItems = searchItems;
        }

        #region 公有方法

        /// <summary>
        /// 转化为Sql语句
        /// </summary>
        /// <returns>Sql字符串</returns>
        public String ConvertToSqlStatement()
        {
            var queryStr = new StringBuilder();

            foreach (var item in this.SearchItems)
            {
                queryStr.Append(this.ConvertSearchItem(item));
            }

            String result = queryStr.ToString();

            #region 防止JavaScript Sql注入式攻击

            String[] keyWords = { "select", "insert", "delete", "count(", "drop table","drop", "update", "truncate", "asc(", "mid(", "char(", "xp_cmdshell", "exec",
                "master", "net",  "where" };//"and", "or",

            for (int i = 0; i < keyWords.Length; i++)
            {
                if (result.IndexOf(keyWords[i], StringComparison.OrdinalIgnoreCase) > 0)
                {
                    String pattern = String.Format(@"[\W]{0}[\W]", keyWords[i]);
                    Regex rx = new Regex(pattern, RegexOptions.IgnoreCase);
                    if (rx.IsMatch(result))
                    {
                        throw new RuYiAdminCustomException(WarnningMessage.SqlInjectionAttackMessage);
                    }
                }
            }

            #endregion

            return this.ReplaceWarnningItems(result);
        }

        /// <summary>
        /// 过滤非关键字
        /// </summary>
        /// <param name="value">sql</param>
        /// <returns>过滤后的sql</returns>
        public String ReplaceWarnningItems(String value)
        {
            //value = value.Replace("'", "");
            value = value.Replace(";", "");
            value = value.Replace("--", "");
            value = value.Replace("/**/", "");

            return value;
        }

        /// <summary>
        /// 追加缺省查询条件
        /// </summary>
        public void AddDefaultSearchItem()
        {
            if (this.SearchItems.Count > 0)
            {
                this.SearchItems.Add(SearchItem.GetDefault());
            }
            else
            {
                this.SearchItems = new List<SearchItem>();
                this.SearchItems.Add(SearchItem.GetDefault());
            }
        }

        /// <summary>
        /// 查询条件转Lamda表达式
        /// </summary>
        /// <typeparam name="T">数据类型</typeparam>
        /// <returns>表达式</returns>
        public Expression<Func<T, bool>> ConvertToLamdaExpression<T>()
        {
            Expression<Func<T, bool>> where = PredicateExtension.True<T>();

            foreach (var searchItem in this.SearchItems)
            {
                var field = searchItem.Field;
                var searchMethod = searchItem.SearchMethod;
                var dataType = searchItem.DataType;
                var value = searchItem.Value;

                if (String.IsNullOrEmpty(field) || value == null)
                {
                    continue;
                }

                Expression constant = null;
                //构建参数
                var parameter = Expression.Parameter(typeof(T), "p");
                //表达式左侧 like: p.Name
                Expression left = Expression.PropertyOrField(parameter, field);
                //表达式右侧，比较值， like '张三'
                Expression right = Expression.Constant(value);

                #region 特殊值处理

                #region 处理Json

                if (right.Type.Equals(typeof(System.Text.Json.JsonElement)))
                {
                    right = Expression.Constant(value.ToString());
                }

                #endregion

                #region 处理操作类型

                if (left.Type.Equals(typeof(OperationType)))
                {
                    right = Expression.Constant((OperationType)int.Parse(value.ToString()), typeof(OperationType));
                }

                #endregion

                #region 处理日期时间

                else if (left.Type.Equals(typeof(DateTime)) && searchMethod != SearchMethod.BetweenAnd)
                {
                    right = Expression.Constant(DateTime.Parse(value.ToString()), typeof(DateTime));
                }
                else if (left.Type.Equals(typeof(DateTime?)) && searchMethod != SearchMethod.BetweenAnd)
                {
                    right = Expression.Constant(DateTime.Parse(value.ToString()), typeof(DateTime?));
                }

                #endregion

                #region 处理Guid

                else if (left.Type.Equals(typeof(Guid)))
                {
                    right = Expression.Constant(Guid.Parse(value.ToString()), typeof(Guid));
                }
                else if (left.Type.Equals(typeof(Guid?)))
                {
                    right = Expression.Constant(Guid.Parse(value.ToString()), typeof(Guid?));
                }

                #endregion

                #region 处理float

                else if (left.Type.Equals(typeof(float)))
                {
                    right = Expression.Constant(float.Parse(value.ToString()), typeof(float));
                }
                else if (left.Type.Equals(typeof(float?)))
                {
                    right = Expression.Constant(float.Parse(value.ToString()), typeof(float?));
                }

                #endregion

                #region 处理Double

                else if (left.Type.Equals(typeof(double)))
                {
                    right = Expression.Constant(double.Parse(value.ToString()), typeof(double));
                }
                else if (left.Type.Equals(typeof(double?)))
                {
                    right = Expression.Constant(double.Parse(value.ToString()), typeof(double?));
                }

                #endregion

                #region 处理int

                else if (left.Type.Equals(typeof(int)))
                {
                    right = Expression.Constant(int.Parse(value.ToString()), typeof(int));
                }
                else if (left.Type.Equals(typeof(int?)))
                {
                    right = Expression.Constant(int.Parse(value.ToString()), typeof(int?));
                }

                #endregion

                #region 处理字符串

                else
                {
                    right = Expression.Constant(value.ToString(), typeof(String));
                }

                #endregion

                #endregion

                switch (searchMethod)
                {
                    case SearchMethod.Equal:
                        constant = Expression.Equal(left, right);
                        break;

                    case SearchMethod.LessThan:
                        constant = Expression.LessThan(left, right);
                        break;

                    case SearchMethod.LessThanOrEqual:
                        constant = Expression.LessThanOrEqual(left, right);
                        break;

                    case SearchMethod.GreaterThan:
                        constant = Expression.GreaterThan(left, right);
                        break;

                    case SearchMethod.GreaterThanOrEqual:
                        constant = Expression.GreaterThanOrEqual(left, right);
                        break;

                    case SearchMethod.BetweenAnd:
                        var arr = value.ToString().Split(',');

                        var lower = dataType == DataType.DateTime ?
                            Expression.Constant(Convert.ToDateTime(arr[0]), typeof(DateTime)) :
                            Expression.Constant(arr[0], typeof(String));

                        var higher = dataType == DataType.DateTime ?
                            Expression.Constant(Convert.ToDateTime(arr[1]), typeof(DateTime)) :
                            Expression.Constant(arr[1], typeof(String));

                        constant = Expression.GreaterThanOrEqual(left, lower);
                        var lambda = Expression.Lambda<Func<T, Boolean>>(constant, parameter);
                        @where = @where.And(lambda);

                        constant = Expression.LessThanOrEqual(left, higher);
                        lambda = Expression.Lambda<Func<T, Boolean>>(constant, parameter);
                        @where = @where.And(lambda);
                        constant = null;
                        break;

                    case SearchMethod.Like:
                    case SearchMethod.Include:
                        var method = dataType == DataType.Int ?
                            typeof(List<int>).GetMethod("Contains", new Type[] { typeof(List<int>) }) :
                            typeof(String).GetMethod("Contains", new Type[] { typeof(String) });

                        constant = Expression.Call(left, method, right);
                        break;

                    case SearchMethod.OrLike:
                        var methodOrLike = dataType == DataType.Int ?
                            typeof(List<int>).GetMethod("Contains", new Type[] { typeof(List<int>) }) :
                            typeof(String).GetMethod("Contains", new Type[] { typeof(String) });

                        constant = Expression.Call(left, methodOrLike, right);
                        lambda = Expression.Lambda<Func<T, Boolean>>(constant, parameter);
                        @where = @where.Or(lambda);

                        constant = null;
                        break;

                    case SearchMethod.NotEqual:
                        constant = Expression.NotEqual(left, right);
                        break;

                    default:
                        break;
                }

                if (constant != null)
                {
                    var lambda = Expression.Lambda<Func<T, Boolean>>(constant, parameter);
                    @where = @where.And(lambda);
                }
            }
            return @where;
        }

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="value"></param>
        /// <returns>SearchCondition</returns>
        public static SearchCondition DeserializeObject(string value)
        {
            return JsonConvert.DeserializeObject<SearchCondition>(value);
        }

        /// <summary>
        /// 序列化
        /// </summary>
        /// <returns>json字符串</returns>
        public String ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// 转化为SearchDescriptor
        /// </summary>
        /// <typeparam name="TDocument">class</typeparam>
        /// <returns>SearchDescriptor</returns>
        public SearchDescriptor<TDocument> ConvertToSearchDescriptor<TDocument>() where TDocument : class
        {
            var searchDescriptor = new SearchDescriptor<TDocument>();

            #region 转化查询条件

            List<SearchItem> searchItems = this.SearchItems;
            if (searchItems != null && searchItems.Count > 0)
            {
                foreach (SearchItem searchItem in searchItems)
                {
                    switch (searchItem.SearchMethod)
                    {
                        case SearchMethod.Equal:
                            if (searchItem.DataType == DataType.Int)
                            {
                                int intValue = int.Parse(searchItem.Value.ToString());
                                searchDescriptor.PostFilter(t => t.Term(x => x.Field(searchItem.Field).Value(intValue)));
                            }
                            else if (searchItem.DataType == DataType.Double)
                            {
                                double doubleValue = double.Parse(searchItem.Value.ToString());
                                searchDescriptor.PostFilter(t => t.Term(x => x.Field(searchItem.Field).Value(doubleValue)));
                            }
                            else
                            {
                                searchDescriptor.PostFilter(t => t.Term(x => x.Field(searchItem.Field).Value(searchItem.Value.ToString())));
                            }
                            break;
                        case SearchMethod.Like:
                            searchDescriptor.PostFilter(t => t.Wildcard(x => x.Field(searchItem.Field).Value("*" + searchItem.Value.ToString() + "*")));
                            break;
                        case SearchMethod.LessThan:
                            searchDescriptor.PostFilter(t => t.Range(x => x.Field(searchItem.Field).LessThan((double?)searchItem.Value)));
                            break;
                        case SearchMethod.LessThanOrEqual:
                            searchDescriptor.PostFilter(t => t.Range(x => x.Field(searchItem.Field).LessThanOrEquals((double?)searchItem.Value)));
                            break;
                        case SearchMethod.GreaterThan:
                            searchDescriptor.PostFilter(t => t.Range(x => x.Field(searchItem.Field).GreaterThan((double?)searchItem.Value)));
                            break;
                        case SearchMethod.GreaterThanOrEqual:
                            searchDescriptor.PostFilter(t => t.Range(x => x.Field(searchItem.Field).GreaterThanOrEquals((double?)searchItem.Value)));
                            break;
                        case SearchMethod.BetweenAnd:
                            Object[] array = searchItem.Value.ToString().Split(",");
                            searchDescriptor.PostFilter(t => t.Range(x => x.Field(searchItem.Field).GreaterThanOrEquals((double?)array[0]).
                            LessThanOrEquals((double?)array[1])));
                            break;
                        case SearchMethod.Include:
                            searchDescriptor.PostFilter(x => x.QueryString(t => t.Fields(f => f.Field(searchItem.Field)).
                            Query(searchItem.Value.ToString().Replace(",", " "))));
                            break;
                        case SearchMethod.OrLike:
                            searchDescriptor.Query(q => q.Bool(b => b.Should(m => m.QueryString(x => x.Fields(t =>
                            t.Field(searchItem.Field)).Query(searchItem.Value.ToString())))));
                            break;
                        case SearchMethod.NotEqual:
                            searchDescriptor.Query(q => q.Bool(b => b.MustNot(m => m.QueryString(x => x.Fields(t =>
                            t.Field(searchItem.Field)).Query(searchItem.Value.ToString())))));
                            break;
                        default:
                            break;
                    }
                }
            }

            #endregion

            #region 转化排序条件

            if (!String.IsNullOrEmpty(this.Sort))
            {
                String column, direction;
                String[] array;
                String[] orders = this.Sort.Split(",");
                foreach (String sort in orders)
                {
                    array = sort.Split(" ");
                    column = array[0];
                    direction = array[1];
                    if (!String.IsNullOrEmpty(direction))
                    {
                        if (direction.Equals("ASC", StringComparison.CurrentCultureIgnoreCase))
                        {
                            searchDescriptor.Sort(t => t.Field(column, SortOrder.Ascending));
                        }
                        else if (direction.Equals("DESC", StringComparison.CurrentCultureIgnoreCase))
                        {
                            searchDescriptor.Sort(t => t.Field(column, SortOrder.Descending));
                        }
                    }
                }
            }

            #endregion

            //分页
            searchDescriptor.Skip(this.PageIndex * this.PageSize).Take(this.PageSize);

            return searchDescriptor;
        }

        #endregion

        #region 私有方法

        /// <summary>
        /// 条件转化
        /// </summary>
        /// <param name="searchItem">查询条件</param>
        /// <returns>字符串</returns>
        private StringBuilder ConvertSearchItem(SearchItem searchItem)
        {
            var condition = new StringBuilder();

            switch (searchItem.SearchMethod)
            {
                case SearchMethod.Equal:
                    if (searchItem.DataType.Equals(DataType.Date))
                    {
                        condition.Append($" and {searchItem.Field}={RuYiDateUtil.ParseToDate(DateTime.Parse(searchItem.Value.ToString()))}");
                    }
                    else if (searchItem.DataType.Equals(DataType.DateTime))
                    {
                        condition.Append($" and {searchItem.Field}={RuYiDateUtil.ParseToDateTime(DateTime.Parse(searchItem.Value.ToString()))}");
                    }
                    else
                    {
                        condition.Append($" and {searchItem.Field}='{searchItem.Value}'");
                    }
                    break;

                case SearchMethod.Like:
                    condition.Append($" and {searchItem.Field} like '%{searchItem.Value}%'");
                    break;

                case SearchMethod.LessThan:
                    condition.Append($" and {searchItem.Field}<'{searchItem.Value}'");
                    break;

                case SearchMethod.LessThanOrEqual:
                    condition.Append($" and {searchItem.Field}<='{searchItem.Value}'");
                    break;

                case SearchMethod.GreaterThan:
                    condition.Append($" and {searchItem.Field}>'{searchItem.Value}'");
                    break;

                case SearchMethod.GreaterThanOrEqual:
                    condition.Append($" and {searchItem.Field}>='{searchItem.Value}'");
                    break;

                case SearchMethod.BetweenAnd:
                    var array = searchItem.Value.ToString().Split(',');
                    if (searchItem.DataType.Equals(DataType.Date))
                    {
                        condition.Append($" and {searchItem.Field} between {RuYiDateUtil.ParseToDate(DateTime.Parse(array[0]))} "
                             + $" and {RuYiDateUtil.ParseToDate(DateTime.Parse(array[1]))}");
                    }
                    else if (searchItem.DataType.Equals(DataType.DateTime))
                    {
                        condition.Append($" and {searchItem.Field} between {RuYiDateUtil.ParseToDateTime(DateTime.Parse(array[0]))} "
                            + $" and {RuYiDateUtil.ParseToDateTime(DateTime.Parse(array[1]))}");
                    }
                    else
                    {
                        condition.Append($" and {searchItem.Field} between '{array[0]}' and '{array[1]}'");
                    }
                    break;

                case SearchMethod.Include:
                    condition.Append($" and {searchItem.Field} in ({searchItem.Value})");
                    break;

                case SearchMethod.OrLike:
                    condition.Append($" or {searchItem.Field} like '%{searchItem.Value}%'");
                    break;

                case SearchMethod.NotEqual:
                    condition.Append($" and {searchItem.Field} <> '{searchItem.Value}'");
                    break;

                default: break;
            }

            return condition;
        }

        #endregion
    }
}
