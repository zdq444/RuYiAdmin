﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using System;

namespace RuYiAdmin.Net.CommonInfrastructure.Constants.Business
{
    /// <summary>
    /// 作业操作常量
    /// </summary>
    public class QuartzJobAction
    {
        public const String DELETE = "Delete";
        public const String START = "Start";
        public const String PAUSE = "Pause";
        public const String RESUME = "Resume";
    }
}
