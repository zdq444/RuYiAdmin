﻿namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// SignalR配置
    /// </summary>
    public class SignalRConfig
    {
        /// <summary>
        /// 审计日志是否开启
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// pattern
        /// </summary>
        public string Pattern { get; set; }

        /// <summary>
        /// method
        /// </summary>
        public string Method { get; set; }
    }
}
