﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// Elasticsearch配置
    /// </summary>
    public class ElasticsearchConfig
    {
        /// <summary>
        /// Elasticsearch URL
        /// </summary>
        public string Uri { get; set; }

        /// <summary>
        /// 默认索引
        /// </summary>
        public string DefaultIndex { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
    }
}
