﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// 数据库配置
    /// </summary>
    public class DBConfig
    {
        /// <summary>
        /// 数据库类型
        /// </summary>
        public int DBType { get; set; }

        /// <summary>
        /// 数据库连接串
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// 数据库超时时间
        /// </summary>
        public int CommandTimeOut { get; set; }

        /// <summary>
        /// 数据库备份路径
        /// </summary>
        public string BackupPath { get; set; }

        /// <summary>
        /// 读写分离从库连接串
        /// </summary>
        public string SlaveConnectionString { get; set; }

        /// <summary>
        /// 读写分离从库2连接串
        /// </summary>
        public string SlaveConnectionString2 { get; set; }

        /// <summary>
        /// 是否自动构建数据库
        /// </summary>
        public bool AutomaticallyBuildDatabase { get; set; }

        /// <summary>
        /// 脚本路径
        /// </summary>
        public string SqlScriptPath { get; set; }
    }
}
