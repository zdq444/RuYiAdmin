﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.Extensions.Configuration;

namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// 全局配置
    /// </summary>
    public class RuYiGlobalConfig
    {
        /// <summary>
        /// 数据库配置
        /// </summary>
        public static DBConfig DBConfig { get; set; }

        /// <summary>
        /// 系统配置
        /// </summary>
        public static SystemConfig SystemConfig { get; set; }

        /// <summary>
        /// 全局配置
        /// </summary>
        public static IConfiguration Configuration { get; set; }

        /// <summary>
        /// Redis配置
        /// </summary>
        public static RedisConfig RedisConfig { get; set; }

        /// <summary>
        /// ActiveMQ配置
        /// </summary>
        public static ActiveMQConfig ActiveMQConfig { get; set; }

        /// <summary>
        /// Jwt配置
        /// </summary>
        public static JwtSettings JwtSettings { get; set; }

        /// <summary>
        /// 审计日志设置
        /// </summary>
        public static LogConfig LogConfig { get; set; }

        /// <summary>
        /// Smtp邮件配置
        /// </summary>
        public static MailConfig MailConfig { get; set; }

        /// <summary>
        /// 系统目录配置
        /// </summary>
        public static DirectoryConfig DirectoryConfig { get; set; }

        /// <summary>
        /// 定时任务配置
        /// </summary>
        public static QuartzConfig QuartzConfig { get; set; }

        /// <summary>
        /// RestSharp配置
        /// </summary>
        public static RestSharpConfig RestSharpConfig { get; set; }

        /// <summary>
        /// Polly配置
        /// </summary>
        public static PollyConfig PollyConfig { get; set; }

        /// <summary>
        /// Consul配置
        /// </summary>
        public static ConsulConfig ConsulConfig { get; set; }

        /// <summary>
        /// 系统并发配置
        /// </summary>
        public static ConcurrencyLimiterConfig ConcurrencyLimiterConfig { get; set; }

        /// <summary>
        /// SmartThreadPool配置
        /// </summary>
        public static SmartThreadPoolConfig SmartThreadPoolConfig { get; set; }

        /// <summary>
        /// 系统缓存配置
        /// </summary>
        public static SystemCacheConfig SystemCacheConfig { get; set; }

        /// <summary>
        /// 消息中间件配置
        /// </summary>
        public static MomConfig MomConfig { get; set; }

        /// <summary>
        /// RabbitMQ配置
        /// </summary>
        public static RabbitMQConfig RabbitMQConfig { get; set; }

        /// <summary>
        /// MongoDB配置
        /// </summary>
        public static MongoDBConfig MongoDBConfig { get; set; }

        /// <summary>
        /// Elasticsearch配置
        /// </summary>
        public static ElasticsearchConfig ElasticsearchConfig { get; set; }

        /// <summary>
        /// Apollo客户端配置
        /// </summary>
        public static ApolloConfig ApolloConfig { get; set; }

        /// <summary>
        /// AspNetCoreRateLimit配置
        /// </summary>
        public static RateLimitConfig RateLimitConfig { get; set; }

        /// <summary>
        /// MiniProfiler配置
        /// </summary>
        public static MiniProfilerConfig MiniProfilerConfig { get; set; }

        /// <summary>
        /// Cap分布式事务配置
        /// </summary>
        public static CapConfig CapConfig { get; set; }

        /// <summary>
        /// Kafka配置
        /// </summary>
        public static KafkaConfig KafkaConfig { get; set; }

        /// <summary>
        /// SignalR配置
        /// </summary>
        public static SignalRConfig SignalRConfig { get; set; }

        /// <summary>
        /// 代码生成器配置
        /// </summary>
        public static CodeGeneratorConfig CodeGeneratorConfig { get; set; }

        /// <summary>
        /// Meilisearch配置
        /// </summary>
        public static MeilisearchConfig MeilisearchConfig { get; set; }

        /// <summary>
        /// Ftp配置
        /// </summary>
        public static FtpConfig FtpConfig { get; set; }

        /// <summary>
        /// SshNet配置
        /// </summary>
        public static SshNetConfig SshNetConfig { get; set; }

        /// <summary>
        /// SM2配置
        /// </summary>
        public static SM2Config SM2Config { get; set; }

        /// <summary>
        /// SM4配置
        /// </summary>
        public static SM4Config SM4Config { get; set; }

        /// <summary>
        /// 多租户配置
        /// </summary>
        public static TenantsConfig TenantsConfig { get; set; }

        /// <summary>
        /// 全局路由模板
        /// </summary>
        public const string RouteTemplate = "API/[controller]/[action]";
    }
}
