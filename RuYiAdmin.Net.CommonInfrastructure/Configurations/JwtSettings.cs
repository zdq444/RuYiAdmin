﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// Jwt配置信息
    /// </summary>
    public class JwtSettings
    {
        /// <summary>
        /// 是否启用JWT
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// 订阅者
        /// </summary>
        public string Audience { get; set; }

        /// <summary>
        /// 发起人
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// 密钥
        /// </summary>
        public string SecurityKey { get; set; }

        /// <summary>
        /// 缺省用户
        /// </summary>
        public string DefaultUser { get; set; }

        /// <summary>
        /// 缺省密码
        /// </summary>
        public string DefaultPassword { get; set; }

        /// <summary>
        /// 盐有效时间（秒）
        /// </summary>
        public int SaltExpiration { get; set; }

        /// <summary>
        /// token有效时间（分钟）
        /// </summary>
        public int TokenExpiration { get; set; }
    }
}
