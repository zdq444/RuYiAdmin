﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// ActiveMQ配置
    /// </summary>
    public class ActiveMQConfig
    {
        /// <summary>
        /// ActiveMQ连接串
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// 主题名称
        /// </summary>
        public string TopicName { get; set; }

        /// <summary>
        /// 队列名称
        /// </summary>
        public string QueueName { get; set; }
    }
}
