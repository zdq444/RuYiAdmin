﻿using RuYiAdmin.Net.CommonInfrastructure.Classes;
using System.Collections.Generic;

namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// 多租户配置
    /// </summary>
    public class TenantsConfig
    {
        /// <summary>
        /// 租户列表
        /// </summary>
        public List<TenantConfig> TenantsList { get; set; }
    }
}
