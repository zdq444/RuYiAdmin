﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// RestSharp配置类
    /// </summary>
    public class RestSharpConfig
    {
        public int TimeOut { get; set; }
    }
}
