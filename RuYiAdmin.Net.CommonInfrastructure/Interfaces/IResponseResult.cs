﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using System;

namespace RuYiAdmin.Net.CommonInfrastructure.Interfaces
{
    /// <summary>
    /// 通用返回接口
    /// </summary>
    public interface IResponseResult<T>
    {
        T DeserializeObject(string value);

        String ToJson();

        T Success(Object obj);

        T OK();

        T BadRequest();

        T BadRequest(String message);

        T Unauthorized();

        T Unauthorized(String message);

        T Forbidden();

        T Forbidden(String message);

        T NotFound();

        T NotFound(String message);

        T NoContent();

        T NoContent(String message);
    }
}
