﻿using Meilisearch;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using System;

namespace RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts
{
    /// <summary>
    /// Meilisearch工具类
    /// </summary>
    public static class RuYiMeilisearchContext
    {
        /// <summary>
        /// Lazy对象
        /// </summary>
        private static readonly Lazy<MeilisearchClient> LazyInstance = new Lazy<MeilisearchClient>(() =>
        {
            var client = new MeilisearchClient(RuYiGlobalConfig.MeilisearchConfig.URL, RuYiGlobalConfig.MeilisearchConfig.ApiKey);
            return client;
        });

        /// <summary>
        /// 单例对象
        /// </summary>
        public static MeilisearchClient Instance { get { return LazyInstance.Value; } }

        /// <summary>
        /// 是否已创建
        /// </summary>
        public static bool IsInstanceCreated { get { return LazyInstance.IsValueCreated; } }

        /// <summary>
        /// 获取Index
        /// </summary>
        /// <param name="meilisearch">MeilisearchClient</param>
        /// <returns>Meilisearch.Index</returns>
        public static Meilisearch.Index GetIndex(this MeilisearchClient meilisearch)
        {
            return Instance.Index(RuYiGlobalConfig.MeilisearchConfig.Index);
        }
    }
}
