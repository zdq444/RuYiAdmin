﻿using Renci.SshNet;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using System;

namespace RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts
{
    /// <summary>
    /// SshNet工具类
    /// </summary>
    public class RuYiSshNetContext
    {
        /// <summary>
        /// SftpClient Lazy对象
        /// </summary>
        private static readonly Lazy<SftpClient> LazySftpInstance = new Lazy<SftpClient>(() =>
        {
            SftpClient sftpClient = null;

            if (RuYiGlobalConfig.SshNetConfig.SftpConfig.Port > 0)
            {
                sftpClient = new SftpClient(RuYiGlobalConfig.SshNetConfig.SftpConfig.Host,
                                            RuYiGlobalConfig.SshNetConfig.SftpConfig.Port,
                                            RuYiGlobalConfig.SshNetConfig.SftpConfig.UserName,
                                            RuYiGlobalConfig.SshNetConfig.SftpConfig.Password);
            }
            else
            {
                sftpClient = new SftpClient(RuYiGlobalConfig.SshNetConfig.SftpConfig.Host,
                                            RuYiGlobalConfig.SshNetConfig.SftpConfig.UserName,
                                            RuYiGlobalConfig.SshNetConfig.SftpConfig.Password);
            }

            return sftpClient;
        });

        /// <summary>
        /// SftpClient单例对象
        /// </summary>
        public static SftpClient SftpInstance { get { return LazySftpInstance.Value; } }

        /// <summary>
        /// SftpClient是否已创建
        /// </summary>
        public static bool IsSftpInstanceCreated { get { return LazySftpInstance.IsValueCreated; } }

        /// <summary>
        /// SshClient Lazy对象
        /// </summary>
        private static readonly Lazy<SshClient> LazySshInstance = new Lazy<SshClient>(() =>
        {
            SshClient sshClient = null;

            if (RuYiGlobalConfig.SshNetConfig.SshConfig.Port > 0)
            {
                sshClient = new SshClient(RuYiGlobalConfig.SshNetConfig.SshConfig.Host,
                                          RuYiGlobalConfig.SshNetConfig.SshConfig.Port,
                                          RuYiGlobalConfig.SshNetConfig.SshConfig.UserName,
                                          RuYiGlobalConfig.SshNetConfig.SshConfig.Password);
            }
            else
            {
                sshClient = new SshClient(RuYiGlobalConfig.SshNetConfig.SshConfig.Host,
                                          RuYiGlobalConfig.SshNetConfig.SshConfig.UserName,
                                          RuYiGlobalConfig.SshNetConfig.SshConfig.Password);
            }

            return sshClient;
        });

        /// <summary>
        /// SshClient单例对象
        /// </summary>
        public static SshClient SshInstance { get { return LazySshInstance.Value; } }

        /// <summary>
        /// SshClient是否已创建
        /// </summary>
        public static bool IsSshInstanceCreated { get { return LazySshInstance.IsValueCreated; } }

        /// <summary>
        /// ScpClient Lazy对象
        /// </summary>
        private static readonly Lazy<ScpClient> LazyScpInstance = new Lazy<ScpClient>(() =>
        {
            ScpClient scpClient = null;

            if (RuYiGlobalConfig.SshNetConfig.ScpConfig.Port > 0)
            {
                scpClient = new ScpClient(RuYiGlobalConfig.SshNetConfig.ScpConfig.Host,
                                          RuYiGlobalConfig.SshNetConfig.ScpConfig.Port,
                                          RuYiGlobalConfig.SshNetConfig.ScpConfig.UserName,
                                          RuYiGlobalConfig.SshNetConfig.ScpConfig.Password);
            }
            else
            {
                scpClient = new ScpClient(RuYiGlobalConfig.SshNetConfig.ScpConfig.Host,
                                          RuYiGlobalConfig.SshNetConfig.ScpConfig.UserName,
                                          RuYiGlobalConfig.SshNetConfig.ScpConfig.Password);
            }

            return scpClient;
        });

        /// <summary>
        /// ScpClient 单例对象
        /// </summary>
        public static ScpClient ScpInstance { get { return LazyScpInstance.Value; } }

        /// <summary>
        /// ScpClient 是否已创建
        /// </summary>
        public static bool IsScpInstanceCreated { get { return LazyScpInstance.IsValueCreated; } }
    }
}
