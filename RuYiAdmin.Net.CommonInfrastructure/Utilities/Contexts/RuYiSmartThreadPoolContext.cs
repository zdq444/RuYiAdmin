﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Amib.Threading;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using System;

namespace RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts
{
    /// <summary>
    /// SmartThreadPool工具类
    /// </summary>
    public class RuYiSmartThreadPoolContext
    {
        /// <summary>
        /// 私有化构造函数
        /// 用于单例模式
        /// </summary>
        private RuYiSmartThreadPoolContext() { }

        /// <summary>
        /// Lazy对象
        /// </summary>
        private static readonly Lazy<SmartThreadPool> LazyInstance = new Lazy<SmartThreadPool>(() =>
        {
            var stp = new SmartThreadPool(
                RuYiGlobalConfig.SmartThreadPoolConfig.IdleTimeout,
                RuYiGlobalConfig.SmartThreadPoolConfig.MaxThreads,
                RuYiGlobalConfig.SmartThreadPoolConfig.MinThreads
                );
            stp.Name = RuYiGlobalConfig.SmartThreadPoolConfig.Name;
            //stp.Concurrency = GlobalContext.SmartThreadPoolConfig.Concurrency;
            stp.CreateWorkItemsGroup(RuYiGlobalConfig.SmartThreadPoolConfig.WorkItemsGroup);
            return stp;
        });

        /// <summary>
        /// 单例对象
        /// </summary>
        public static SmartThreadPool Instance { get { return LazyInstance.Value; } }

        /// <summary>
        /// 是否已创建
        /// </summary>
        public static bool IsInstanceCreated { get { return LazyInstance.IsValueCreated; } }
    }
}
