﻿using FluentFTP;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using System;

namespace RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts
{
    /// <summary>
    /// Ftp工具类
    /// </summary>
    public class RuYiFtpContext
    {
        /// <summary>
        /// Lazy对象
        /// </summary>
        private static readonly Lazy<FtpClient> LazyInstance = new Lazy<FtpClient>(() =>
        {
            FtpClient client = null;

            if (!String.IsNullOrEmpty(RuYiGlobalConfig.FtpConfig.UserName)
             && !String.IsNullOrEmpty(RuYiGlobalConfig.FtpConfig.Password))
            {
                if (RuYiGlobalConfig.FtpConfig.Port > 0)
                {
                    client = new FtpClient(RuYiGlobalConfig.FtpConfig.Host,
                                           RuYiGlobalConfig.FtpConfig.UserName,
                                           RuYiGlobalConfig.FtpConfig.Password,
                                           RuYiGlobalConfig.FtpConfig.Port);
                }
                else
                {
                    client = new FtpClient(RuYiGlobalConfig.FtpConfig.Host,
                                           RuYiGlobalConfig.FtpConfig.UserName,
                                           RuYiGlobalConfig.FtpConfig.Password);
                }
            }
            else
            {
                if (RuYiGlobalConfig.FtpConfig.Port > 0)
                {
                    client = new FtpClient(RuYiGlobalConfig.FtpConfig.Host, RuYiGlobalConfig.FtpConfig.Port);
                }
                else
                {
                    client = new FtpClient(RuYiGlobalConfig.FtpConfig.Host);
                }
            }

            return client;
        });

        /// <summary>
        /// 单例对象
        /// </summary>
        public static FtpClient Instance { get { return LazyInstance.Value; } }

        /// <summary>
        /// 是否已创建
        /// </summary>
        public static bool IsInstanceCreated { get { return LazyInstance.IsValueCreated; } }
    }
}
