﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils
{
    /// <summary>
    /// Hash函数工具类
    /// </summary>
    public class RuYiHashUtil
    {
        /// <summary>
        /// SHA256加密
        /// </summary>
        /// <param name="input">输入</param>
        /// <returns>输出</returns>
        public static byte[] HashData256(string input)
        {
            byte[] hash;
            using (SHA256 sha256 = SHA256.Create())
            {
                hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(input));
            }
            return hash;
        }

        /// <summary>
        /// SHA256加密
        /// </summary>
        /// <param name="input">输入</param>
        /// <returns>输出</returns>
        public static string HashToHexString256(string input)
        {
            byte[] hash;
            using (SHA256 sha256 = SHA256.Create())
            {
                hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(input));
            }
            return Convert.ToHexString(hash);
        }

        /// <summary>
        /// SHA512加密
        /// </summary>
        /// <param name="input">输入</param>
        /// <returns>输出</returns>
        public static byte[] HashData512(string input)
        {
            byte[] hash;
            using (SHA512 sha512 = SHA512.Create())
            {
                hash = sha512.ComputeHash(Encoding.UTF8.GetBytes(input));
            }
            return hash;
        }

        /// <summary>
        /// SHA512加密
        /// </summary>
        /// <param name="input">输入</param>
        /// <returns>输出</returns>
        public static string HashToHexString512(string input)
        {
            byte[] hash;
            using (SHA512 sha512 = SHA512.Create())
            {
                hash = sha512.ComputeHash(Encoding.UTF8.GetBytes(input));
            }
            return Convert.ToHexString(hash);
        }
    }
}
