﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;
using System.Linq;

namespace RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils
{
    /// <summary>
    /// Request Header工具类
    /// </summary>
    public static class RuYiHeaderUtil
    {
        /// <summary>
        /// 获取Header中指定key的值
        /// </summary>
        /// <param name="context">HttpContext对象</param>
        /// <param name="key">键</param>
        /// <returns>值</returns>
        public static string GetHeaderValue(this HttpContext context, string key)
        {
            var token = string.Empty;

            token = context.Request.Headers[key].FirstOrDefault();

            return token;
        }
    }
}
