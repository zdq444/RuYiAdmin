﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Enums.Business
{
    /// <summary>
    /// 角色类型枚举
    /// </summary>
    public enum RoleType
    {
        /// <summary>
        /// 自有的
        /// </summary>
        Own,

        /// <summary>
        /// 继承的
        /// </summary>
        Inherited
    }
}
