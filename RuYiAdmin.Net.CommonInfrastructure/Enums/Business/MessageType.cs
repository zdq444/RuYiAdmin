﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Enums.Business
{
    /// <summary>
    /// 消息类型
    /// </summary>
    public enum MessageType
    {
        /// <summary>
        /// 强制下线
        /// </summary>
        ForceLogout,

        /// <summary>
        /// 通知
        /// </summary>
        Notification,

        /// <summary>
        /// 公告
        /// </summary>
        Announcement,

        /// <summary>
        /// 广播
        /// </summary>
        Broadcast
    }
}
