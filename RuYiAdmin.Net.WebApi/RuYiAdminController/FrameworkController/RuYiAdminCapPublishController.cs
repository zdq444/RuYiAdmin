﻿using DotNetCore.CAP;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminFilter;
using System;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.FrameworkController
{
    [Authorize]
    [ActionAuthorization]
    [ApiController]
    [Route(RuYiGlobalConfig.RouteTemplate)]
    public class RuYiAdminCapPublishController : ControllerBase
    {
        private readonly ICapPublisher capPublisher;

        public RuYiAdminCapPublishController(ICapPublisher capPublisher)
        {
            this.capPublisher = capPublisher;
        }

        [HttpPost]
        public IActionResult SendMessage()
        {
            var header = new Dictionary<string, string>()
            {
                ["my.header.first"] = "first",
                ["my.header.second"] = "second"
            };

            capPublisher.Publish("RuYiAdmin.CapService.ShowTime", DateTime.Now, header);

            return Ok();
        }
    }
}
