﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.FrameworkController
{
    /// <summary>
    /// 健康检查控制器
    /// </summary>
    [Route(RuYiGlobalConfig.RouteTemplate)]
    [Produces("application/json")]
    [ApiController]
    public class RuYiAdminHealthController : ControllerBase
    {
        /// <summary>
        /// 健康检查
        /// </summary>
        /// <returns>OkObjectResult</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Get()
        {
            return await Task.Run(() =>
            {
                return Ok("ok");
            });
        }
    }
}
