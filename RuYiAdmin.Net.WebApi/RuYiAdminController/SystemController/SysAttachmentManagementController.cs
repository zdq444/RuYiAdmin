﻿using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminAnnotation;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminBase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.SystemController
{
    /// <summary>
    /// 业务附件管理控制器
    /// </summary>
    public class SysAttachmentManagementController : RuYiAdminBaseController<SysAttachment>
    {
        #region 属性及构造函数

        /// <summary>
        /// 业务附件业务接口实例
        /// </summary>
        private readonly ISysAttachmentService AttachmentService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="AttachmentService"></param>
        public SysAttachmentManagementController(ISysAttachmentService AttachmentService) : base(AttachmentService)
        {
            this.AttachmentService = AttachmentService;
        }

        #endregion

        #region 上传业务附件

        /// <summary>
        /// 上传业务附件
        /// </summary>
        /// <param name="businessId">业务编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.UploadFile)]
        public async Task<IActionResult> UploadAttachments(Guid businessId)
        {
            var attachments = new List<SysAttachment>();

            foreach (var item in this.HttpContext.Request.Form.Files)
            {
                var attachment = new SysAttachment();

                attachment.Create(this.HttpContext);
                attachment.FileName = item.FileName;
                //处理文件大小
                string remark = string.Empty;
                attachment.FileSize = RuYiFileContext.GetFileSize(item, out remark);
                attachment.Remark = remark;
                //保存文件
                attachment.FilePath = RuYiFileContext.SaveBusinessAttachment(item, attachment.Id.ToString());
                //附件关联业务
                attachment.BusinessId = businessId;

                attachments.Add(attachment);
            }

            var actionResponseResult = await this.AttachmentService.AddListAsync(attachments, false);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 获取业务附件

        /// <summary>
        /// 获取业务附件
        /// </summary>
        /// <param name="businessId">业务编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{businessId}")]
        [Log(OperationType.QueryList)]
        public async Task<IActionResult> GetAttachments(Guid businessId)
        {
            var searchCondition = new SearchCondition();
            searchCondition.SearchItems = new List<SearchItem>();
            searchCondition.SearchItems.Add(new SearchItem()
            {
                Field = "BusinessId",
                DataType = DataType.Guid,
                SearchMethod = SearchMethod.Equal,
                Value = businessId.ToString()
            });
            //获取附件信息
            var actionResponseResult = await this.AttachmentService.GetListAsync(searchCondition);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 下载业务附件

        /// <summary>
        /// 下载业务附件
        /// </summary>
        /// <param name="attachmentId">附件编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{attachmentId}")]
        [Log(OperationType.DownloadFile)]
        public async Task<IActionResult> DownloadAttachment(Guid attachmentId)
        {
            //获取附件信息
            var task = await this.AttachmentService.GetByIdAsync(attachmentId);
            var attachment = (SysAttachment)task.Object;
            //存储路径
            var filePath = attachment.FilePath;
            //文件读写流
            var stream = new FileStream(filePath, FileMode.Open);
            //设置流的起始位置
            stream.Position = 0;
            return File(stream, "application/octet-stream", attachment.FileName);
        }

        #endregion

        #region 删除业务附件

        /// <summary>
        /// 删除业务附件
        /// </summary>
        /// <param name="attachmentIds">数组串</param>
        /// <returns>ActionResponseResult</returns>
        [HttpDelete("{attachmentIds}")]
        [Log(OperationType.DeleteEntity)]
        public async Task<IActionResult> DeleteAttachment(string attachmentIds)
        {
            var array = RuYiStringUtil.GetGuids(attachmentIds);
            var actionResponseResult = await this.AttachmentService.DeleteRangeAsync(array);
            //删除业务文件
            foreach (var item in array)
            {
                var attachment = this.AttachmentService.GetById(item).Object as SysAttachment;
                RuYiFileContext.DeleteFile(attachment.FilePath);
            }
            return Ok(actionResponseResult);
        }

        #endregion

        #region 系统文件统计

        /// <summary>
        /// 系统文件统计
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        [HttpGet]
        [Log(OperationType.QueryEntity)]
        [Permission("attachment:query:list")]
        public async Task<IActionResult> GetSysFileStatisticalInfo()
        {
            //获取统计信息
            var actionResponseResult = await this.AttachmentService.QuerySysFileStatisticalInfo();
            return Ok(actionResponseResult);
        }

        #endregion
    }
}
