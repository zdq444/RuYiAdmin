﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.FrameworkModel;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminAnnotation;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminFilter;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.SystemController
{
    [Authorize]
    [ActionAuthorization]
    [ApiController]
    [Route(RuYiGlobalConfig.RouteTemplate)]
    public class SysCodeGeneratorController : ControllerBase
    {
        #region 属性及其构造函数

        /// <summary>
        /// 代码生成器接口实例
        /// </summary>
        private readonly ISysCodeGeneratorService CodeGeneratorService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="CodeGeneratorService"></param>
        public SysCodeGeneratorController(ISysCodeGeneratorService CodeGeneratorService)
        {
            this.CodeGeneratorService = CodeGeneratorService;
        }

        #endregion

        #region 获取表空间信息

        /// <summary>
        /// 获取表空间信息
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        public async Task<IActionResult> Post()
        {
            var list = await this.CodeGeneratorService.GetSchemaInfo();
            var actionResponseResult = QueryResponseResult<SchemaInfoDTO>.Success(list.Count, list);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 生成代码

        /// <summary>
        /// 生成代码
        /// </summary>
        /// <param name="codeGenerator">生成器对象</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.GenerateCode)]
        public async Task<IActionResult> CodeGenerate([FromBody] CodeGeneratorDTO codeGenerator)
        {
            var result = await this.CodeGeneratorService.CodeGenerate(codeGenerator);
            var actionResponseResult = ActionResponseResult.Success(result);
            return Ok(actionResponseResult);
        }

        #endregion
    }
}
