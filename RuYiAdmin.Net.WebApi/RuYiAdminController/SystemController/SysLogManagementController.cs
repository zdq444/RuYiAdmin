﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Meilisearch;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Extensions.Business;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.FrameworkInterface;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminAnnotation;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminBase;
using SqlSugar;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.SystemController
{
    /// <summary>
    /// 审计日志管理控制器
    /// </summary>
    public class SysLogManagementController : RuYiAdminBaseController<SysLog>
    {
        #region 属性及构造函数

        /// <summary>
        /// 审计日志接口实例
        /// </summary>
        private readonly ISysLogService LogService;

        /// <summary>
        /// MongoDB接口实例
        /// </summary>
        private readonly IMongoDBService MongoDBService;

        /// <summary>
        /// Nest接口实例
        /// </summary>
        private readonly INestService NestService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="LogService"></param>
        /// <param name="MongoDBService"></param>
        /// <param name="NestService"></param>
        public SysLogManagementController(ISysLogService LogService,
                                          IMongoDBService MongoDBService,
                                          INestService NestService) : base(LogService)
        {
            this.LogService = LogService;
            this.MongoDBService = MongoDBService;
            this.NestService = NestService;
        }

        #endregion

        #region 查询日志列表

        /// <summary>
        /// 查询日志列表
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        [Permission("log:query:list")]
        public async Task<IActionResult> Post([FromBody] SearchCondition searchCondition)
        {
            if (RuYiGlobalConfig.LogConfig.SupportMongoDB)
            {
                #region 从MongoDB获取审计日志

                IMongoCollection<SysLog> mongoCollection = MongoDBService.GetBusinessCollection<SysLog>("SysLog");

                var logs = mongoCollection.AsQueryable().Where(searchCondition.ConvertToLamdaExpression<SysLog>()).ToList();
                if (!string.IsNullOrEmpty(searchCondition.Sort))
                {
                    logs = logs.Sort(searchCondition.Sort);
                }

                var queryList = logs.Skip(searchCondition.PageIndex * searchCondition.PageSize).Take(searchCondition.PageSize).ToList();
                var actionResponseResult = QueryResponseResult<SysLog>.Success(logs.Count, queryList);

                return Ok(actionResponseResult);

                #endregion
            }
            else if (RuYiGlobalConfig.LogConfig.SupportElasticsearch)
            {
                #region 从Elasticsearch获取审计日志         

                //**********如果使用keyword从Elasticsearch查询审计日志，请使用以下代码*********************************//

                ////前端只传递keyword
                //var keyword = searchCondition.QueryItems.FirstOrDefault().Value.ToString();

                ////模糊查询
                //var task1 = await RuYiEsNestContext.Instance.SearchAsync<SysLog>(s => 
                //s.Query(q => q.Match(m => m.Field(f => f.UserName).Query(keyword)) || 
                //             q.Match(m => m.Field(f => f.OrgName).Query(keyword))||
                //             q.Match(m => m.Field(f => f.IP).Query(keyword))||
                //             q.Match(m => m.Field(f => f.OperationType).Query(keyword))||
                //             q.Match(m => m.Field(f => f.RequestMethod).Query(keyword))||
                //             q.Match(m => m.Field(f => f.RequestUrl).Query(keyword))||
                //             q.Match(m => m.Field(f => f.Params).Query(keyword))||
                //             q.Match(m => m.Field(f => f.NewValue).Query(keyword))||
                //             q.Match(m => m.Field(f => f.OldValue).Query(keyword)) ||
                //             q.Match(m => m.Field(f => f.Result).Query(keyword)) ||
                //             q.Match(m => m.Field(f => f.Remark).Query(keyword))
                //       )
                // );

                //var logs1 = task1.Documents.ToList();

                //if (!String.IsNullOrEmpty(searchCondition.Sort))
                //{
                //    logs1 = logs1.Sort<SysLog>(searchCondition.Sort);
                //}
                //logs1 = logs1.Skip(searchCondition.PageIndex * searchCondition.PageSize).Take(searchCondition.PageSize).ToList();

                //var actionResult1 = QueryResponseResult<SysLog>.Success((int)task1.Total, logs1);

                //return Ok(actionResult1);

                //**********如果使用keyword从Elasticsearch查询审计日志，请使用以上代码查询*********************************//

                //转化查询条件
                var query = searchCondition.ConvertToSearchDescriptor<SysLog>();
                //查询Elasticsearch
                var searchResponse = await NestService.GetElasticClient().SearchAsync<SysLog>(query);
                var logs = searchResponse.Documents.ToList();
                var actionResponseResult = QueryResponseResult<SysLog>.Success((int)searchResponse.Total, logs);

                return Ok(actionResponseResult);

                #endregion
            }
            else if (RuYiGlobalConfig.LogConfig.SupportMeilisearch)
            {
                #region 从Meilisearch获取审计日志  

                //前端只传递keyword
                var keyword = searchCondition.SearchItems.FirstOrDefault().Value.ToString();
                var index = RuYiMeilisearchContext.Instance.GetIndex();

                ISearchable<SysLog> searchResult = await index.SearchAsync<SysLog>(keyword);

                var hits = searchResult.Hits.ToList();
                if (!string.IsNullOrEmpty(searchCondition.Sort))
                {
                    hits = hits.Sort(searchCondition.Sort);
                }
                var logs = hits.Skip(searchCondition.PageIndex * searchCondition.PageSize).Take(searchCondition.PageSize).ToList();

                var actionResponseResult = QueryResponseResult<SysLog>.Success(hits.Count, logs);
                return Ok(actionResponseResult);

                #endregion
            }
            else
            {
                #region 从关系库获取审计日志

                var key = "sqls:sql:query_syslog_ms";
                if ((DbType)RuYiGlobalConfig.DBConfig.DBType == DbType.MySql)
                {
                    key = "sqls:sql:query_syslog";
                }

                var actionResponseResult = await this.LogService.SqlQueryAsync(searchCondition, key);

                return Ok(actionResponseResult);

                #endregion
            }
        }

        #endregion

        #region 获取日志明细

        /// <summary>
        /// 获取日志明细
        /// </summary>
        /// <param name="logId">日志编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{logId}")]
        [Log(OperationType.QueryEntity)]
        [Permission("log:query:entity")]
        public async Task<IActionResult> GetById(Guid logId)
        {
            var actionResponseResult = await this.LogService.GetByIdAsync(logId);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 下载返回数据

        /// <summary>
        /// 下载返回数据
        /// </summary>
        /// <param name="txtId">日志编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{txtId}")]
        [Log(OperationType.DownloadFile)]
        public async Task<IActionResult> DownloadMonitoringLog(string txtId)
        {
            return await Task.Run(() =>
            {
                //存储路径
                var path = Path.Join(RuYiGlobalConfig.DirectoryConfig.GetMonitoringLogsPath(), "/");
                //文件名称
                var fileName = txtId + ".txt";
                //文件路径
                var filePath = Path.Join(path, fileName);
                //文件读写流
                var stream = new FileStream(filePath, FileMode.Open);
                //设置流的起始位置
                stream.Position = 0;

                return File(stream, "application/octet-stream", fileName);
            });
        }

        #endregion
    }
}
