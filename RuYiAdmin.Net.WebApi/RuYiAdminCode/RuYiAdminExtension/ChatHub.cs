﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.SignalR;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminExtension
{
    public class ChatHub : Hub
    {
        /// <summary>
        /// 给连接的所有人发送消息
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public Task SendMsg(string userName, string message)
        {
            //方法需要在前端实现
            return Clients.All.SendAsync(RuYiGlobalConfig.SignalRConfig.Method, userName, message);
        }
    }
}
