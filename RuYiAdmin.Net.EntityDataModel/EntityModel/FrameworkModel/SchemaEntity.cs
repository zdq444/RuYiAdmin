﻿using System.Collections.Generic;

namespace RuYiAdmin.Net.EntityDataModel.EntityModel.FrameworkModel
{
    /// <summary>
    /// 实体模型
    /// </summary>
    public class SchemaEntity
    {
        /// <summary>
        /// 无参构造函数
        /// </summary>
        public SchemaEntity()
        {
            Fields = new List<SchemaField>();
        }

        /// <summary>
        /// 有参构造函数
        /// </summary>
        /// <param name="name"></param>
        public SchemaEntity(string name) : this()
        {
            EntityName = name;
        }

        /// <summary>
        /// 实体名称
        /// </summary>
        public string EntityName { get; set; }

        /// <summary>
        /// 字段集
        /// </summary>
        public List<SchemaField> Fields { get; set; }

        /// <summary>
        /// 首字母转大写
        /// </summary>
        /// <param name="input">输入</param>
        /// <returns>输出</returns>
        public string ToTitleCase(string input)
        {
            var output = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(input.ToLower());
            return output;
        }
    }
}
