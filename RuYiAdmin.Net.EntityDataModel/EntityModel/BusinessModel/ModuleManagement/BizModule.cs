//-----------------------------------------------------------------------
// <Copyright file="BizModule.cs" company="RuYiAdmin">
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// * Version : 4.0.30319.42000
// * Author  : auto generated by RuYiAdmin T4 Template
// * FileName: BizModule.cs
// * History : Created by RuYiAdmin 01/19/2022 09:01:54
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.BaseEntityModel;
using SqlSugar;
using System.ComponentModel.DataAnnotations;

namespace RuYiAdmin.Net.EntityDataModel.EntityModel.BusinessModel.ModuleManagement
{
    /// <summary>
    /// BizModule Entity Model
    /// </summary>   
    //[Serializable]
    [SugarTable("biz_module")]
    public class BizModule : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 模块名称
        /// </summary>
        [Required]
        [MaxLength(512)]
        [SugarColumn(ColumnName = "MODULE_NAME")]
        public string ModuleName { get; set; }

        /// <summary>
        /// 模块简称
        /// </summary>
        [Required]
        [MaxLength(256)]
        [SugarColumn(ColumnName = "MODULE_SHORT_NAME")]
        public string ModuleShortName { get; set; }

        /// <summary>
        /// 模块英文简称
        /// </summary>
        [Required]
        [MaxLength(128)]
        [SugarColumn(ColumnName = "MODULE_SHORT_NAME_EN")]
        public string ModuleShortNameEN { get; set; }

        /// <summary>
        /// 采用HTTP协议，HTTP或者HTTPS
        /// </summary>
        [Required]
        [MaxLength(15)]
        [SugarColumn(ColumnName = "MODULE_PROTOCOL")]
        public string ModuleProtocol { get; set; }

        /// <summary>
        /// 模块地址：ip或者域名
        /// </summary>
        [Required]
        [MaxLength(256)]
        [SugarColumn(ColumnName = "MODULE_ADDRESS")]
        public string ModuleAddress { get; set; }

        /// <summary>
        /// 模块端口
        /// </summary>
        [SugarColumn(ColumnName = "MODULE_PORT")]
        public int? ModulePort { get; set; }

        /// <summary>
        /// 模块logo图片位置
        /// </summary>
        //[Required]
        //[MaxLength(512)]
        [SugarColumn(ColumnName = "MODULE_LOGO_ADDRESS")]
        public string ModuleLogoAddress { get; set; }

        /// <summary>
        /// 模块单点登录地址
        /// </summary>
        //[Required]
        [MaxLength(512)]
        [SugarColumn(ColumnName = "MODULE_SSO_ADDRESS")]
        public string ModuleSsoAddress { get; set; }

        /// <summary>
        /// 模块待办地址
        /// </summary>
        //[Required]
        [MaxLength(512)]
        [SugarColumn(ColumnName = "MODULE_TODO_ADDRESS")]
        public string ModuleTodoAddress { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [SugarColumn(ColumnName = "SERIAL_NUMBER")]
        public int? SerialNumber { get; set; }
    }
}
