﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.BaseEntityModel;
using SqlSugar;
using System;
using System.ComponentModel.DataAnnotations;

namespace RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel
{
    /// <summary>
    /// 角色用户关系模型
    /// </summary>
    [SugarTable("sys_role_user")]
    public class SysRoleUser : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 角色编号
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "ROLE_ID")]
        public Guid RoleId { get; set; }

        /// <summary>
        /// 用户编号
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "USER_ID")]
        public Guid UserId { get; set; }
    }
}
