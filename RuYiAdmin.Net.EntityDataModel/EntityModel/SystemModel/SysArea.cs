﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.BaseEntityModel;
using SqlSugar;
using System.ComponentModel.DataAnnotations;

namespace RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel
{
    /// <summary>
    /// 行政区域模型
    /// </summary>
    [SugarTable("sys_area")]
    public class SysArea : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 地区编码
        /// </summary>
        [Required, MaxLength(6)]
        [SugarColumn(ColumnName = "AREA_CODE")]
        public string AreaCode { get; set; }

        /// <summary>
        /// 父地区编码
        /// </summary>
        [Required, MaxLength(6)]
        [SugarColumn(ColumnName = "PARENT_AREA_CODE")]
        public string ParentAreaCode { get; set; }

        /// <summary>
        /// 地区名称
        /// </summary>
        [Required, MaxLength(50)]
        [SugarColumn(ColumnName = "AREA_NAME")]
        public string AreaName { get; set; }

        /// <summary>
        /// 邮政编码
        /// </summary>
        [Required, MaxLength(50)]
        [SugarColumn(ColumnName = "ZIP_CODE")]
        public string ZipCode { get; set; }

        /// <summary>
        /// 地区层级(1省份 2城市 3区县)
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "AREA_LEVEL")]
        public int AreaLevel { get; set; }
    }
}
