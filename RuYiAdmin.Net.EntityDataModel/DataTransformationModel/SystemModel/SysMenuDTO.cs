﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using System.Collections.Generic;

namespace RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel
{
    /// <summary>
    /// 菜单DTO
    /// </summary>
    public class SysMenuDTO : SysMenu
    {
        /// <summary>
        /// 子集
        /// </summary>
        public List<SysMenuDTO> Children { get; set; }

        /// <summary>
        /// 英文菜单名称
        /// </summary>
        public string MenuNameEn { get; set; }

        /// <summary>
        /// 俄文菜单名称
        /// </summary>
        public string MenuNameRu { get; set; }
    }
}
