﻿using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;

namespace RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel
{
    /// <summary>
    /// 通知公告DTO
    /// </summary>
    public class SysAnnouncementDTO : SysAnnouncement
    {
        /// <summary>
        /// 收件人列表
        /// </summary>
        public string Addressee { get; set; }

        /// <summary>
        /// 附件编号
        /// </summary>
        public string AttachmentIds { get; set; }

        /// <summary>
        /// 是否发送邮件
        /// </summary>
        public bool SendMail { get; set; }
    }
}
